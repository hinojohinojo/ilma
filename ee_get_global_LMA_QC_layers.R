library(rgee)

ee_Initialize(user = "chinojoh@email.arizona.edu")


#########
# HEADER
#########

#Function to mask clouds and snow based on the pixel_qa band of Landsat 8 SR data.
maskl578 <- function(image) {
  # Bits 2, 3, 4 and 5 are water, cloud shadow, cloud, and snow, respectively.
  cloudShadowBitMask <- bitwShiftL(1, 3)
  cloudBitMask <- bitwShiftL(1, 5)
  snowBitMask <- bitwShiftL(1, 4)
  waterBitMask <- bitwShiftL(1, 2)
  # Get the pixel QA band
  pixelQA <- image$select("pixel_qa")
  # Both flags should be set to zero, indicating clear conditions.
  mask <- pixelQA$bitwiseAnd(cloudShadowBitMask)$eq(0)$
                 And(pixelQA$bitwiseAnd(cloudBitMask)$eq(0))$
                 And(pixelQA$bitwiseAnd(snowBitMask)$eq(0))$
                 And(pixelQA$bitwiseAnd(waterBitMask)$eq(0))
  return(image$updateMask(mask))
}

# Function to harmonize the different Landsats
# according to coefficients in Roy et al 2016 DOI http://dx.doi.org/10.1016/j.rse.2015.12.024
harmonizationRoy <- function(image) {
  slopes <- ee$Image$constant(c(0.8850, 0.9317, 0.9372, 0.8339, 0.8639, 1, 0.9165))  # RMA - create an image of slopes per band for L8 TO L7 regression line - David Roy
  itcp <- ee$Image$constant(c(0.0183, 0.0123, 0.0123, 0.0448, 0.0306, 0, 0.0116))  # RMA - create an image of y-intercepts per band for L8 TO L7 regression line - David Roy
  y <- image$select(c("B2","B3","B4","B5","B6","B10","B7"), c("B1", "B2", "B3", "B4", "B5", "B6", "B7"))$  # select OLI bands 2-7 and rename them to match L7 band names
    resample("bicubic")$  # ...resample the L8 bands using bicubic
    multiply(slopes)$add(itcp$multiply(10000))$  # multiply the y-intercept bands by 10000 to match the scale of the L7 bands then apply the line equation
    set("system:time_start", image$get("system:time_start"))  # set the output system:time_start metadata to the input image time_start otherwise it is null
    #set("system:time_end", oli.get("system:time_end"))  # set the output system:time_end metadata to the input image time_end otherwise it is null
  return(y$toShort())  # return the image as short to match the type of the other data
}




# Functions for vegetation indices
addNDVI_l5 <- function(image) {
  ndvi <- image$normalizedDifference(c("B4", "B3"))$rename("NDVI")
  return(image$addBands(ndvi))
}

addNIRv_l5 <- function(image) {
  NIRv <- image$select("NDVI")$multiply(image$select("B4")$divide(10000))$rename("NIRv")
  return(image$addBands(NIRv))
}

addNDMI_l5 <- function(image) {
  ndmi <- image$normalizedDifference(c("B4", "B5"))$rename("NDMI")
  return(image$addBands(ndmi))
}

# Function to calculate iLMA
addiLMA <- function(image) {
  iLMA <- image$expression('iLMA = (b("NDMI") + 0.51) / b("NIRv")')
  return(image$addBands(iLMA))
}

# Function to calculate LMA from iLMA
addLMA <- function(image) {
  LMA <- image$expression('LMA = -0.0045886 + (0.0043203 * b("iLMA"))')
  return(image$addBands(LMA))
}

# Function to add DOY from an image collection
addDate <- function(image) {
  doy <- image$date()$getRelative("day", "year")
  doyBand <- ee$Image$constant(doy)$add(1)$uint16()$rename("DOY")
  doyBand <- doyBand$updateMask(image$select("NDMI")$mask())
  
  return(image$addBands(doyBand))
}

# Function to convert DOY to radians
daysToRad <- function(image) {
  rad <- image$select("centerDOY")$
    multiply(360)$
    divide(366)$
    multiply(pi)$
    divide(180)
  return(image$addBands(rad$rename("centerDOYrad")))
}





#########
# PROCESSING
#########

area <- "asia"  # Adjust as needed
regions <- ee$FeatureCollection(paste0("users/hinojohinojo/", area, "_sections_2_5_degrees"))
regionsList <- regions$toList(regions$size())
regionsLength <- regions$size()$getInfo()

# Prepare the mask to mask bare areas
landMask <- ee$ImageCollection("COPERNICUS/Landcover/100m/Proba-V-C3/Global")$
  select("bare-coverfraction")$
  mean()

landMask <- landMask$lte(50)

# Prepare the mask to mask water bodies
#water <- ee.Image("JRC/GSW1_3/GlobalSurfaceWater")$
#waterMask <- water$select("occurrence")$eq(0)$
  #and(water$select("recurrence")$eq(0))$unmask(1, TRUE)	;

#i <- 150  # For testing

##### THE FOR LOOP #####
for(i in seq(0, regionsLength -1, 1)) {
  
  region <- ee$Feature(regionsList$get(i))$geometry()
  block <- ee$Feature(regionsList$get(i))$get('block')$getInfo()
  
  # What years have data
  years5 <- ee$List$sequence(1985, 2011, 1)$
    map(ee_utils_pyfunc(function(item) {
      YOI <- ee$Number(item)$uint16()
      YOIst <- ee$String(YOI)
      feat <- ee$Feature(NULL, list(year = YOI))
      yearCol <- ee$ImageCollection("LANDSAT/LT05/C01/T1_SR")$
        filterBounds(region)$
        filterDate(ee$Date$fromYMD(YOI, 1L, 1L), ee$Date$fromYMD(YOI, 12L, 31L))
      size <- ee$Number(yearCol$size())$uint16()
      return(feat$set(list(n = size)))
    }))

  years7 <- ee$List(list(2012))$
    map(ee_utils_pyfunc(function(item) {
      YOI <- ee$Number(item)$uint16()
      feat <- ee$Feature(NULL, list(year = YOI))
      yearCol <- ee$ImageCollection("LANDSAT/LE07/C01/T1_SR")$
        filterBounds(region)$
        filterDate(ee$Date$fromYMD(YOI, 1L, 1L), ee$Date$fromYMD(YOI, 12L, 31L))
      size <- ee$Number(yearCol$size())$uint16()
      return(feat$set(list(n = size)))
    }))

  years8 <- ee$List$sequence(2013, 2019, 1)$
    map(ee_utils_pyfunc(function(item) {
      YOI <- ee$Number(item)$uint16()
      feat <- ee$Feature(NULL, list(year = YOI))
      yearCol <- ee$ImageCollection("LANDSAT/LC08/C01/T1_SR")$
        filterBounds(region)$
        filterDate(ee$Date$fromYMD(YOI, 1L, 1L), ee$Date$fromYMD(YOI, 12L, 31L))
        size <- ee$Number(yearCol$size())$uint16()
      return(feat$set(list(n = size)))
  }))
  
  years <- years5$cat(years7)$cat(years8)
  years <- ee$FeatureCollection(years)

  # Keep only years with more than that amount of images
  yearsWithData <- years$filter(ee$Filter$gte("n", 3));
  
  # The processing of the images
  LMAsCollection <- yearsWithData$map(function(feat){
    #feat <- ee$Feature(yearsWithData$toList(yearsWithData$size())$get(2))  # For testing
    YOIint <- ee$Number(feat$get("year"))$uint16()  # Year Of Interest
    YOIst <- ee$String(YOIint)
        
    # Load the correct landsat data for each year/step
    landsat <- ee$Algorithms$If(YOIint$lte(2011),
      ee$ImageCollection("LANDSAT/LT05/C01/T1_SR")$
	filterBounds(region)$
        filterDate(ee$Date$fromYMD(YOIint, 1L, 1L), ee$Date$fromYMD(YOIint, 12L, 31L))$
        map(maskl578)$
        select("B3","B4","B5"),
            
      ee$Algorithms$If(YOIint$eq(2012),
	ee$ImageCollection("LANDSAT/LT05/C01/T1_SR")$
	  filterBounds(region)$
	  filterDate(ee$Date$fromYMD(YOIint, 1L, 1L), ee$Date$fromYMD(YOIint, 4L, 30L))$
	  map(maskl578)$
	  select("B3","B4","B5")$
	  merge(
	    ee$ImageCollection("LANDSAT/LE07/C01/T1_SR")$
	      filterBounds(region)$
	      filterDate(ee$Date$fromYMD(YOIint, 5L, 1L), ee$Date$fromYMD(YOIint, 12L, 31L))$
	      map(maskl578)$
	      select("B3","B4","B5")
	  ),
	ee$Algorithms$If(YOIint$gte(2013),
	  ee$ImageCollection("LANDSAT/LC08/C01/T1_SR")$
	    filterDate(ee$Date$fromYMD(YOIint, 1L, 1L), ee$Date$fromYMD(YOIint, 12L, 31L))$
	    filterBounds(region)$
	    map(maskl578)$
	    map(harmonizationRoy)$
	    select("B3","B4","B5")
	)
      )
    )
    
    landsat <- ee$ImageCollection(landsat)$
      map(addNDVI_l5)$
      map(addNIRv_l5)$
      map(addNDMI_l5)$
      map(addiLMA)$
      map(addLMA)$
      select("LMA", "NDMI", "NIRv")
      
    # Here is the moving average thing
    # I used the moving average to make the map with the mean LMA
    # around (two months) the peak NIRv value
    join <- ee$Join$saveAll(
      matchesKey = "images"
    )
    
    timeField <- "system:time_start"
    timeDiff <- 1000 * 60 * 60 * 24 * 30  # This makes a window of 2 months around each date/image

    
    diffFilter <- ee$Filter$maxDifference(
      difference = timeDiff,
      leftField = timeField,
      rightField = timeField
    )
    
    threeNeighborJoin = join$apply(
      primary = landsat, 
      secondary = landsat, 
      condition = diffFilter
    )
    
    smoothed <- ee$ImageCollection(threeNeighborJoin$map(function(image) {
      landsat <- ee$ImageCollection$fromImages(image$get("images"))
      return(ee$Image(image)$addBands(landsat$median()))
    }))
    # Here it ends the moving average thing
    
    # The following steps add Day of year of images as a band
    # This band is the basis for making the map
    smoothed <- smoothed$map(addDate)

    # Save a copy of the smoothed series so could be used in the QCs of years
    DOYs <- smoothed$select("DOY")
    
    # Here we get the date of the maximum NIRv
    # and make images of the 2 month window around that date
    centerDOY <- smoothed$qualityMosaic("NIRv")$select("DOY")
    startDOY <- centerDOY$subtract(30)
    endDOY <- centerDOY$add(30)
  
    # We mask all data outside the window
    # and make and average of LMA and NDMI within the window
    
    
    # A function to mask images outside a window of dates
    smoothed <- smoothed$map(function(image) {
      doy <- image$select("DOY")
      mask <- doy$gte(startDOY)$And(doy$lte(endDOY))
      return(image$updateMask(mask))
    })
    
    LMAyear <- smoothed$select("LMA_1")$mean()$rename("LMA")
    NDMIyear <- smoothed$select("NDMI_1")$mean()$rename("NDMI")
    nImagesYear <- DOYs$select("DOY")$reduce(ee$Reducer$countDistinctNonNull())
  
  
    # Convert LMA from g cm-2 to g m-2 units
    LMAyear <- LMAyear$multiply(10000)
  
    # Apply the correction to cross-calibrate Landsats
    # Comment out if these lines if the correction is not needed
    #LMAyear <- ee$Algorithms$If(YOIint$lt(2012),  # Correction for Landsat 5
    #  LMAyear$multiply(0.858488)$add(13.007041),

    #  ee$Algorithms$If(YOIint$gt(2012),  # Correction for Landsat 8
    #    LMAyear$multiply(1.05687)$add(-2.03858),

    #    LMAyear$multiply(1)))  # No Correction for Landsat 7
        
    # Convert LMA image to unsigned integer, to reduce the resulting
    # image filesize
    # also adds the date as metadata, year as band and a constant band
    # year and constant bands are used in the regression step
    LMAyear <- ee$Image(LMAyear)$toShort()$
      set("system:time_start", ee$Date$fromYMD(YOIint, 1L, 1L)$millis())$
      addBands(centerDOY$toShort()$rename("centerDOY"))$
      addBands(nImagesYear$toShort()$rename("nImagesYear"))$
      addBands(ee$Image$constant(YOIint)$toUint16()$rename("year"))
        
    # Mask all LMA taken at low LAI, which is done with using a value
    # as an NDMI threshold (0-0.1)
    NDMImask <- NDMIyear$gte(0)
    LMAyear <- LMAyear$updateMask(NDMImask)

    # Also mask all bare areas
    LMAyear <- LMAyear$updateMask(landMask)

    # Add the NDVI layer
    LMAyear <- LMAyear$addBands(NDMIyear$multiply(10000)$toShort())


    return(LMAyear)


  })



  LMAsCollection <- ee$ImageCollection(LMAsCollection)$
    map(daysToRad)

  # General QC layers
  DOYvariance <- LMAsCollection$select("centerDOYrad")$
    reduce(ee$Reducer$circularVariance())$
    multiply(10000)$
    toShort()$
    rename("DOYvariance")
    
  yearsWithData <- LMAsCollection$select("LMA")$
    reduce(ee$Reducer$count())$
    toShort()$
    rename("yearsWithData")

  minYear <- LMAsCollection$select("year")$
    reduce(ee$Reducer$min())$
    toShort()$
    rename("minYear")

  maxYear <- LMAsCollection$select("year")$
    reduce(ee$Reducer$max())$
    toShort()$
    rename("maxYear")

  rangeOfYears <- maxYear$subtract(minYear)$
    toShort()$
    rename("rangeOfYears")

  NDMImean <- LMAsCollection$select("NDMI")$
    mean()$
    toShort()$
    rename("NDMImean")

  # Put together all general QC layers and define its no-data value
  QCs <- yearsWithData$unmask(-32768)$
    addBands(rangeOfYears$unmask(-32768))$
    addBands(DOYvariance$unmask(-32768))$
    addBands(NDMImean$unmask(-32768))



  # QC layers for the number of data per year
  nImagesAll <- LMAsCollection$select("nImagesYear")$
    filterDate("1985-01-01", "2019-12-31")$
    mean()$
    toShort()$
    rename("nImagesAll")
    
  nImagesAllSd <- LMAsCollection$select("nImagesYear")$
    filterDate("1985-01-01", "2019-12-31")$
    reduce(ee$Reducer(ee$Reducer$stdDev()))$
    toShort()$
    rename("nImagesAllSd")

#  nImages85to89 <- LMAsCollection$select("nImagesYear")$
#    filterDate("1985-01-01", "1989-12-31")$
#    mean()$
#    toShort()$
#    rename("nImages85to89")
#    
#  nImages90to99 <- LMAsCollection$select("nImagesYear")$
#    filterDate("1990-01-01", "1999-12-31")$
#    mean()$
#    toShort()$
#    rename("nImages90to99")
#
#  nImages00to19 <- LMAsCollection$select("nImagesYear")$
#    filterDate("2000-01-01", "2019-12-31")$
#    mean()$
#    toShort()$
#    rename("nImages00to19")

  QCsNdata <- nImagesAll$unmask(-32768)$
    addBands(nImagesAllSd$unmask(-32768))
#    addBands(nImages85to89$unmask(-32768))$
#    addBands(nImages90to99$unmask(-32768))$
#    addBands(nImages00to19$unmask(-32768))


   

#  Map$addLayer(eeObject = DOYvariance,
#    visParams = list(min = 0, max = 10000), 
#    name = "DOYvariance")
#    
#  Map$addLayer(eeObject = yearsWithData,
#    visParams = list(min = 0, max = 35), 
#    name = "yearsWithData")
#    
#  Map$addLayer(eeObject = rangeOfYears,
#    visParams = list(min = 0, max = 35), 
#    name = "rangeOfYears")
#    
#  Map$addLayer(eeObject = NDMImean,
#    visParams = list(min = -3000, max = 6000), 
#    name = "NDMImean")
    
  

  # Export the LMAslope image at 1000 m resolution
#  QCtask <- ee_image_to_drive(
#    image = QCs,
#    description = paste0("QCs_", area, "_", block , "_1000m"),
#    folder = "LMA_QCs",
#    fileNamePrefix = paste0("QCs_", area, "_", block, "_1000m"),
#    timePrefix = FALSE,
#    region = region,
#    scale = 1000L,
#    crs = "EPSG:4326",
#    maxPixels = 15000000000,
#    #shardSize = 1024,
#    fileDimensions = 10240L,
#    fileFormat = "GeoTIFF"
#  )
#
#  QCtask$start()

  QCtask2 <- ee_image_to_drive(
    image = QCsNdata,
    description = paste0("QCsNdata_", area, "_", block , "_1000m"),
    folder = "LMA_QCs",
    fileNamePrefix = paste0("QCsNdata_", area, "_", block, "_1000m"),
    timePrefix = FALSE,
    region = region,
    scale = 1000L,
    crs = "EPSG:4326",
    maxPixels = 15000000000,
    #shardSize = 1024,
    fileDimensions = 10240L,
    fileFormat = "GeoTIFF"
  )

  QCtask2$start()



  print(i)
}
