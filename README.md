# iLMA

## Introduction
Here you can find the code used to process the data and perform the analysis and figures included in the manuscript "Global shift in a key plant trait indicates pervasive alteration of Biosphere functioning". The instructions to run the code can be found on the following sections, after the upcoming notes.

### Some notes
In order to save space, only key intermediate outputs will be provided in this repo or on the OSF repository at https://doi.org/10.17605/OSF.IO/6XENK . Only the lightest data required are kept here, and the heavy data required (e.g. NEON data used or global imagery and intermediate steps imagery) is located at the OSF repository. For that reason, most paths (e.g. file and directory paths) on R and Google Earth Engine scripts needs to be adjusted to whatever location the large files end up being located and named.

A Google Earth Engine account is needed to be able to run any of the Earth Engine scripts, either for the Google Earth Engine client (*.js files) or for the R rgee package (ee_*.R).

Detailed comments are provided on each script, providing more specific guidance on how to use the scripts. We recommend the scripts to be run line-by-line, so the user can more easily follow the instructions, adjust settings or paths as needed, and check outputs and plots as they are being processed.

### Software versions used
The codes were run and tested in R version 4.2.1, and uses the following R packages: raster v3.6-3, sf v1.0-8, tmap v3.3-3, scico v1.3.1, cmocean v0.3-1, rgee v1.1.5, rgdal v1.5-32, googledrive v2.0.0, ggplot2 v3.4.4, plyr v1.8.7, deming v1.4, ggpubr v0.4.0, data.table v1.14.2, rcompanion v2.4.18, viridis v0.6.2, dplyr v1.0.10, reticulate v1.26, lme4 v1.1-30, bbmle v1.0.25, neonUtilities v2.2.1, neonOS v1.0.0, phenofit v0.2.2, lhs v1.1.6, terra v1.6-17, hsdar v1.0.4. The rgee package used Python3 earthengine-api v0.1.323. R figures out and install the required dependencies for each of these packages or provide explicit software package names that may need to be installed manually if needed.

All software was run on a PC with Slackware 15.0 (a Linux distribution), except for the scripts to be run on a High Performance Computer (we used the one at University of Arizona). See also "Installation notes"

### Installation notes
While the codes provided here had been run on a Linux machine, we expect that they can be run on any operating system. R software can be downloaded from https://www.r-project.org/ for a wide range of operating systems, or follow your OS best practices for installing R. All packages can be installed from the CRAN repository (the standard repository for the R packages). rgee package can be tricky to install and run (it has some tricky to install dependencies such as the "reticulate" R package, the Earth Engine Python API and others), but detailed installation instructions can be found at the package website at https://github.com/r-spatial/rgee .

The installation of all the required software can take from half hour or less if installing at Windows or recent Mac from pre-compiled packages, or up to several hours in case the computer needs compilation of the packages.

The instructions for installing the required software to run the R scripts for the High Performance Computer may need to be provided by the HPC staff.

## Testing iLMA with PROSAIL (takes a few minutes)
This is done in two scripts:
1. The script "understanding_c.R" aims to illustrate LAI and LMA (and leaf water content) influence over NDMI and NIRv, and also how varying "c" parameter influence the hability of iLMA to capture the LMA signal from surface reflectance. This script performs PROSAIL simulations to generate the Supplementary Figure 2.
2. Using PROSAIL model, the script "reflectance_simulations.R" tests the iLMA vs LMA relationship and also assess which traits (parameters) cause most residual variation of this relationship. The script generates Figures 1a, and Supplementary Figures 3 and 6.
3. This model analysis acknowledge that LMA and leaf water content are correlated. The script "lma_vs_leaf_water_fromNEON.R" assesses this correlation.

## Testing iLMA with field data (takes about 30 min, maybe less)
1. For the NEON trait and species abundance datasets, which are publicly available, we provide the data used as downloaded from NEON and the code to process them to obtain the abundance-weighted leaf mass per area of the canopy. The data takes a few hundred megabytes, so it was deposited on the OSF repository at https://doi.org/10.17605/OSF.IO/6XENK .
2. The field observations of traits and species abundance for the Colorado and Forest datasets have not been made publicly available. For that reason, we provide the abundance-weighted LMA data already processed. The raw data on traits and abundances and the code to process it can be requested to Cesar Hinojo Hinojo (cesar.hinojo@unison.mx) and Brian J. Enquist (benquist@email.arizona.edu).
3. Get Landsat reflectance data from the field plots locations using the scripts "ee_get_landsat_*_plots_explicitQA.R". The shapefiles containing the locations are stored in the folder "field_plots_locations_layers"
4. Calculate average phenology dates from Landsat data using scripts "phenology_from_*_plots.R".
5. Combine Landsat and field LMA data with the scripts "integrating_landsat_and_LMA_data_*.R".
6. Find out the optimal iLMA's "*c*" constant and calculate the calibration equation using the script "all_datasets_optimal_iLMA_constant.R". This script create Figures 1b and Supplementary Figure 4, and also assessess the relationship between other indices (NIRv and SLAVI) vs LMA shown in Supplementary Figure 7.
7. The test of iLMA ability to capture temporal changes uses the following: 1) obtain Landsat data from the locations where we have temporal field LMA data using the script "ee_get_landsat_temporalTest_sites_explicitQA.R", 2) use the code "LMA_site_temporal_tests.R" to compare field vs satellite estimates at 4 sites and create Figure 3.

## Harmonization of Landsat 5, 7 and 8 (takes some minutes)
1. Random sample locations are taken across the globe, stratified by land cover, through the Google Earth Engine script "locations_by_land_cover_for_QAdatasets.js", which is intended to be used in Google Earth Engine (Java Script) code editor. The resulting feature collection needs to be exported as a shapefile and loaded in the next step.
2. Get yearly LMA data for all overlapping years between the different Landsat at all sample locations using the script "ee_get_global_LMA_samples_per_land_cover_for_QC.R"
3. The harmonization, as well as other quality checks are made in the script "QCs_of_analyses.R".

## Global mapping of LMA and its long-term change (the whole process could take 2-4 weeks)
*Notes on the time to process: Running all this section can take a few weeks. Each section of the world (e.g. North America) for a single layer (e.g. yearly LMA) can take overnight to one day, and there are 6 sections of the world and there are 7 global layers to process.*

1. Split the world in grids. The processing of the images will be made on one grid at at time. The grid is generated with the code "autogenerate_global_sections_for_LMA_map.R". The generated text files contain text that needs to be run in Google Earth Engine java script client to create a Feature collection and each one needs to be saved as an asset. The grids will be used in the following scripts, so make sure the names of the grid layer matches the one on the folowing codes.
2. Get the global LMA layer estimates for year 2019 for all the grids of the world using the script "ee_get_global_LMA_yearly.R" which downloads also a layer for mean growing season NDMI.
3. Get the global layer estimates of LMA long-term change (called LMAslopeSen in the scripts) for all the grids of the world using the script "ee_get_global_LMAslope_sen.R".
4. Get the quality control layers for the global LMA long-term change for all the grids of the world using the script "ee_get_global_LMA_QC_layers.R". 
5. Merge all the grid images for 2019 LMA and NDMI, LMA long-term change and its QC layers, using the script "LMA_map_sections_processing.R". The result of this step is getting a single global image for each of these variables.
6. The resulting images should be uploaded to Google Earth Engine as Assets.
7. The quality control of the 2019 LMA and the LMA long-term change is processed in Google Earth Engine via the script "LMA_map_global_images_processing.R"
8. The plot of map Figures 2a and 2b is made with script "LMA_map_global_figures.R".

To create the boxplot/violin plots from Figure 4. There are several steps involved:

9. Process and export the land cover types across latitudinal bands using the java script for earth engine  "export_landcover.js", which is intended to be used in Google Earth Engine (Java Script) code editor.
10. The global LMA and long-term change images are processed together with the land cover map with script "LMA_global_histograms_preparation". This part needs global layers with processing for "water" from the script "LMA_map_global_images_processing".
11. Then, in a High Performance Computer continue the processing of the resulting images with scripts "LMA_global_histograms_processing_hpc.R" and "LMA_global_histograms_processing2_hpc.R". The figure is made in the last script.

For the regional maps on Supplementary Figures 9 and 10:

12. Get the regional RGBs, LMA and LMA change images using the scripts "ee_get_regions_RGBs.R, "ee_get_regions_LMA.R", "ee_get_regions_LMAslope_sen.R". The grid sections used for each region are: northAmerica 16x26 for Colorado Rocky Mountains, northAmerica 15x21 for Sierra Nevada, northAmerica 23x29 for Manitoba, southAmerica 20x19 for Mato Grosso, northAmerica 12x25 for Sonora, northAmerica 13x32 for Louisiana
13. Create the regional map figure with "regional_maps_preparation.R"

## Analysis of the drivers of LMA long-term change (may take 1 or 2 weeks)
1. Generate a set of random sample locations across the globe stratified by land cover type. This is performed in the earth engine script "locations_by_landcover.js". This script is intended for use within the Google Earth Engine Java script code editor. The resulting feature collection needs to be downloaded as a shapefile and loaded in the following step.
2. Get Landsat and climate (ERA5 Land) data from the random locations, using  scripts "ee_get_global_LMA_samples_per_land_cover.R" and "ee_get_global_climate_samples_per_land_cover_per_latBand.R" and , respectivelly.
3. Combine the Landsat data with yearly climate in script "global_temporal_climate_analisis_per_latBand.R". This code creates Figure 5 and the Supplementary Table 2.
3. The assessment of human impacts is made with earth engine script "exploring_QC_and_human_impacts.js" in the Earth Engine code editor. This script needs global LMA change layer and all the QC layers to be uploaded as assets in earth engine. Depending on the settings chosen within the code, it outputs the results shown in Supplementary Table 3.
