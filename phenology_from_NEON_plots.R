library(plyr)
library(data.table)
library(phenofit)
library(ggplot2)
library(viridis)

#####
# Prepare the data from all plots
#####

files <- list.files(path = "landsat/explicit_QA/")

data <- data.frame()
for (i in 1:length(files)) {
	tmp <- read.csv(paste0("landsat/explicit_QA/", files[i]))
	data <- rbind(data, tmp)
}

data$pathrow <- sprintf(fmt = "%06d", data$pathrow)
data$site <- substr(x = data$plotID, start = 1, stop = 4)
data$plotID_year <- paste(data$plotID, data$year, sep = "_")

# Calculate indices
data$NDVI <- (data$B4 - data$B3) / (data$B4 + data$B3)
data$NDSI <- (data$B2 - data$B5) / (data$B2 + data$B5)
data$EVI <- 2.5 * (((data$B4 / 10000) - (data$B3 / 10000)) / ((data$B4 / 10000) + 6 * (data$B3 / 10000) - 7.5 * (data$B1 / 10000) + 1))

# Set the weights for the different QCs
data$w <- NA
data$QC_flag <- NA

data$w[data$CS == 1] <- 0.5
data$QC_flag[data$CS == 1] <- "shadow"

data$w[data$CL == 1] <- 0.1
data$QC_flag[data$CL == 1] <- "cloud"

#data$w[data$NDSI >= 0] <- 0.1
#data$QC_flag[data$NDSI >= 0] <- "snow"

data$w[data$SN == 1] <- 0.1
data$QC_flag[data$SN == 1] <- "snow"

data$w[data$SN == 0 & data$CL == 0 & data$CS == 0] <- 1
data$QC_flag[data$SN == 0 & data$CL == 0 & data$CS == 0] <- "good"


#####
# Site-specific stuff
#####
leapYears <- seq(1984, 2020, 4)

# BONA is an evergreen

# CLBJ
#site <- "CLBJ"
#siteData <- subset(data, data$site == "CLBJ")
#siteData$jday[siteData$pathrow == "028037"] <- siteData$jday[siteData$pathrow == "028037"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# DCFS
#site <- "DCFS"
#siteData <- subset(data, data$site == "DCFS")
#siteData$jday[siteData$pathrow == "032027"] <- siteData$jday[siteData$pathrow == "032027"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# DEJU is an evergreen

# DELA
#site <- "DELA"
#siteData <- subset(data, data$site == "DELA")
#siteData <- subset(siteData, siteData$pathrow == "021037")
##siteData$jday[siteData$pathrow == "032027"] <- siteData$jday[siteData$pathrow == "032027"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# GRSM
#site <- "GRSM"
#siteData <- subset(data, data$site == "GRSM")
##siteData <- subset(siteData, siteData$pathrow == "021037")
#siteData$jday[siteData$pathrow == "019035"] <- siteData$jday[siteData$pathrow == "019035"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# GUAN is an evergreen

# HARV
#site <- "HARV"
#siteData <- subset(data, data$site == "HARV")
#siteData <- subset(siteData, siteData$pathrow == "013030" | siteData$pathrow == "012031")
#siteData$jday[siteData$pathrow == "013030"] <- siteData$jday[siteData$pathrow == "013030"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# JERC
#site <- "JERC"
#siteData <- subset(data, data$site == "JERC")
#siteData <- subset(siteData, siteData$pathrow == "019038" | siteData$pathrow == "018038")
#siteData$jday[siteData$pathrow == "019038"] <- siteData$jday[siteData$pathrow == "019038"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# JORN
#site <- "JORN"
#siteData <- subset(data, data$site == "JORN")
#siteData <- subset(siteData, siteData$pathrow == "033037")
##siteData$jday[siteData$pathrow == "019038"] <- siteData$jday[siteData$pathrow == "019038"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# KONA
#site <- "KONA"
#siteData <- subset(data, data$site == "KONA")
#siteData <- subset(siteData, siteData$pathrow == "028033")
##siteData$jday[siteData$pathrow == "019038"] <- siteData$jday[siteData$pathrow == "019038"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# KONZ
#site <- "KONZ"
#siteData <- subset(data, data$site == "KONZ")
#siteData <- subset(siteData, siteData$pathrow == "028033")
##siteData$jday[siteData$pathrow == "019038"] <- siteData$jday[siteData$pathrow == "019038"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# LENO
#site <- "LENO"
#siteData <- subset(data, data$site == "LENO")
#siteData <- subset(siteData, siteData$pathrow == "021038")
##siteData$jday[siteData$pathrow == "019038"] <- siteData$jday[siteData$pathrow == "019038"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# MLBS
#site <- "MLBS"
#siteData <- subset(data, data$site == "MLBS")
#siteData <- subset(siteData, siteData$pathrow == "017034")
##siteData$jday[siteData$pathrow == "019038"] <- siteData$jday[siteData$pathrow == "019038"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# MOAB
#site <- "MOAB"
#siteData <- subset(data, data$site == "MOAB")
#siteData <- subset(siteData, siteData$pathrow == "036033")
##siteData$jday[siteData$pathrow == "019038"] <- siteData$jday[siteData$pathrow == "019038"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# OAES
#site <- "OAES"
#siteData <- subset(data, data$site == "OAES")
#siteData <- subset(siteData, siteData$pathrow == "028036" | siteData$pathrow == "029035")
#siteData$jday[siteData$pathrow == "029035"] <- siteData$jday[siteData$pathrow == "029035"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# ORNL
#site <- "ORNL"
#siteData <- subset(data, data$site == "ORNL")
#siteData <- subset(siteData, siteData$pathrow == "019035")
##siteData$jday[siteData$pathrow == "029035"] <- siteData$jday[siteData$pathrow == "029035"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# PUUM is evergreen

# SCBI
#site <- "SCBI"
#siteData <- subset(data, data$site == "SCBI")
#siteData <- subset(siteData, siteData$pathrow == "016033")
##siteData$jday[siteData$pathrow == "029035"] <- siteData$jday[siteData$pathrow == "029035"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# SERC
#site <- "SERC"
#siteData <- subset(data, data$site == "SERC")
#siteData <- subset(siteData, siteData$pathrow == "015033")
##siteData$jday[siteData$pathrow == "029035"] <- siteData$jday[siteData$pathrow == "029035"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# SOAP is evergreen

# STEI
# The data of this site are perfect for phenology, very good curve fits
#site <- "STEI"
#siteData <- subset(data, data$site == "STEI")
#siteData <- subset(siteData, siteData$pathrow == "024028" | siteData$pathrow == "025028")
#siteData$jday[siteData$pathrow == "025028"] <- siteData$jday[siteData$pathrow == "025028"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# TALL is evergreen but has some seasonal pattern
site <- "TALL"
siteData <- subset(data, data$site == "TALL")
siteData <- subset(siteData, siteData$pathrow == "020037" | siteData$pathrow == "021037")
siteData$jday[siteData$pathrow == "021037"] <- siteData$jday[siteData$pathrow == "021037"] + 1
siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
siteData.dates <- subset(siteData.dates, select = -year) 
siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
siteData$date <- as.Date(siteData.datePOSIX)



# TOOL has very bad image coverage and most likely insufficient for phenology extraction. But should be ok to sample this data around jday 200, where the highest VIs occur and the best coverage.

# UKFS
# Phenology only processed until year 2012 due to weird errors
#site <- "UKFS"
#siteData <- subset(data, data$site == "UKFS")
#siteData <- subset(siteData, siteData$pathrow == "027033")
##siteData$jday[siteData$pathrow == "025028"] <- siteData$jday[siteData$pathrow == "025028"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 16))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# UNDE
# Relativelly good data for phenology extraction on most of the years
#site <- "UNDE"
#siteData <- subset(data, data$site == "UNDE")
#siteData <- subset(siteData, siteData$pathrow == "024028" | siteData$pathrow == "025028")
#siteData$jday[siteData$pathrow == "025028"] <- siteData$jday[siteData$pathrow == "025028"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# WOOD
# Good data for phenology extraction on most of the years
#site <- "WOOD"
#siteData <- subset(data, data$site == "WOOD")
#siteData <- subset(siteData, siteData$pathrow == "031027" | siteData$pathrow == "032027")
#siteData$jday[siteData$pathrow == "032027"] <- siteData$jday[siteData$pathrow == "032027"] + 1
#siteData.dates <- ddply(.data = siteData, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 1, -8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates <- ddply(.data = siteData.dates, .variables = c("plotID_year"), .fun = summarise,
#		    jday = seq(min(jday), 366, 8))  # Adjust to steps of 8 (path overlap) or 16 (no path overlap or row overlap)
#siteData.dates$year <- as.numeric(substr(x = siteData.dates$plotID_year, start = 10, stop = 14))
#siteData.dates <- subset(siteData.dates, !(!(siteData.dates$year %in% leapYears) & siteData.dates$jday == 366))
#siteData.dates <- subset(siteData.dates, select = -year) 
#siteData <- join(x = siteData.dates, y = siteData, by = c("plotID_year", "jday"))
#siteData$plotID <- substr(x = siteData$plotID_year, start = 1, stop = 8)
#siteData$year <- as.numeric(substr(x = siteData$plotID_year, start = 10, stop = 14))
#siteData.datePOSIX <- strptime(x = paste(siteData$year, siteData$jday, sep = "/"), format = "%Y/%j", tz = "GMT")
#siteData$date <- as.Date(siteData.datePOSIX)

# WREF is evergreen



ggplot() +
	geom_point(data = siteData[siteData$CL == 0 & siteData$CS == 0 & siteData$SN == 0, ], aes(x = jday, y = EVI), color = "black") +
	geom_point(data = siteData[siteData$CL == 1, ], aes(x = jday, y = EVI), color = "red") +
	geom_point(data = siteData[siteData$CS == 1, ], aes(x = jday, y = EVI), color = "orange") +
	geom_point(data = siteData[siteData$SN == 1, ], aes(x = jday, y = EVI), color = "blue") +
	geom_hline(yintercept = 0.05) +
	coord_cartesian(ylim = c(0, 1)) +
	facet_wrap(~ year)



#####
# Calculate the phenology metrics
#####

# Set some phenofit parameters
alpha <- 0.02
wmin <- 0.1  # Set the minimum weight to the actual minimum weight used in the QC
ymin = 0.05  # minimum value of vegetation index in bare soil. Set it to 0.05 for EVI and 0.075 (or even 0.08) for NDVI
iters <- 3  # Iterations for the curve fits, 2 is good, 3 is better
south <- FALSE  # Sites are in north or south hemisphere. North is FALSE
method <- "AG"  # Could be also Elmore, Elmore gives interesting fits too, but probably not better than AG in most of the phenology extraction methods

plotYearList <- unique(x = siteData$plotID_year)
phen <- data.frame()
phen.fit <- data.frame()
#progressBar <- txtProgressBar(min = 0, max = length(plotYearList), initial = 0, style = 3)
for (i in 1:length(plotYearList)) {
	print(paste0(i, ", ", round(i * 100 / length(plotYearList), digits = 2), "%"))
	plotData <- subset(siteData, siteData$plotID_year == plotYearList[i])
	
	# phenofit requires a siteData.table as an input
	plotData <- setDT(plotData[, c("date", "jday", "EVI", "w", "QC_flag")])
	colnames(plotData)[3] <- "y"
	
	# Set an important parameter for phenofit
	nptperyear <- nrow(plotData)  # How many images are per year

	# Phenofit requires to add one year before and after the siteData analyzed
	plotData.headTail <- add_HeadTail(d = plotData, south = south, nptperyear = nptperyear)

	# Put the siteData in the format required as input for phenofit
	plotData.input <- check_input(t = plotData.headTail$date, y = plotData.headTail$y, w = plotData.headTail$w, QC_flag = plotData.headTail$QC_flag, nptperyear = nptperyear, south = south, maxgap = nptperyear / 4, alpha = alpha, wmin = wmin, date_start = plotData$date[1], date_end = plotData$date[nrow(plotData)], mask_spike = TRUE, ymin = ymin)

	# Set the initial rough season
	plotData.season <- season_mov(INPUT = plotData.input, rFUN = smooth_wWHIT, wFUN = wTSM, maxExtendMonth = 6, r_min = 0.1, IsPlot = FALSE, IsPlot.OnlyBad = FALSE, print = FALSE, iters = iters, calendarYear = TRUE)

	# Fit the curve
	plotData.fit <- curvefits(INPUT = plotData.input, brks = plotData.season, methods = method, wFUN = wTSM, nextend = 2, maxExtendMonth = 3, minExtendMonth = 1, minPercValid = 0.2, iters = iters, wmin = 0.1, print = FALSE)
	# Get the phenology metrics
	#plotData.phen <- get_pheno(fits = plotData.fit)
	plotData.phen <- get_pheno(fits = plotData.fit, IsPlot = TRUE)

	# Save the number of good quality landsat siteData per plot per year
	nGoodData.year <- sum(plotData$QC_flag == "good", na.rm = TRUE)
	nData.winter <- sum(!is.na(plotData$y[plotData$jday <= 90]))
	nData.spring <- sum(!is.na(plotData$y[plotData$jday >= 91 & plotData$jday <= 181]))
	nData.summer <- sum(!is.na(plotData$y[plotData$jday >= 182 & plotData$jday <= 273]))
	nData.fall <- sum(!is.na(plotData$y[plotData$jday >= 274 & plotData$jday <= 366]))

	# Save the goodness of fit data
	plotData.gof <- get_GOF(fit = plotData.fit)

	# Save the fitted time series
	plotData.fittedTs <- cbind(data.frame(sitePlotYear = plotYearList[i]), get_fitting(fit = plotData.fit))

	# Save the phenology metrics and the number of good observations and number of siteData points per season for this plotID_year
	plotData.result <- data.frame(sitePlotYear = plotYearList[i], nptperyear = nptperyear, nGoodData.year = nGoodData.year, nData.winter = nData.winter, nData.spring = nData.spring, nData.summer = nData.summer, nData.fall = nData.fall, plotData.phen$doy[[method]], RMSE = plotData.gof$RMSE, NSE = plotData.gof$NSE, R = plotData.gof$R, pvalue = plotData.gof$pvalue, n = plotData.gof$n)
	phen <- rbind(phen, plotData.result)
	phen.fit <- rbind(phen.fit, plotData.fittedTs)

	# update the progress bar
#	setTxtProgressBar(progressBar, i)
}

# Export the results, phenology metrics for each site_plot_year and the number of good observations for quality control
write.csv(x = phen, file = paste0("landsat_phenology/", site, "_NEON_plots_phenology.csv"), row.names = FALSE)
write.csv(x = phen.fit, file = paste0("landsat_phenology_fits/", site, "_NEON_plots_phenology_fit.csv"), row.names = FALSE)




#####
# Some exploration
#####
sitePlotYear.split <- strsplit(x = phen$sitePlotYear, split = "_")
sitePlotYear.df <- ldply(.data = sitePlotYear.split, .fun = function(x) c(site = x[1], plot = x[2], year = x[3]))
phen2 <- phen
phen2 <- cbind(sitePlotYear.df, phen2)

phen2$year <- as.numeric(phen2$year)

ggplot(data = phen2, mapping = aes(x = nGoodData.year)) + geom_histogram() + facet_wrap(~site)

ggplot(data = phen2[phen2$nGoodData.year > 5, ], mapping = aes(x = year, y = DER.sos, color = nGoodData.year)) +
	geom_point() +
	geom_smooth(method = "lm") +
	scale_color_viridis() +
	facet_wrap(~ site)

ggplot(data = phen2[phen2$nGoodData.year > 5, ], mapping = aes(x = year, y = DER.eos, color = nGoodData.year)) +
	geom_point() +
	geom_smooth(method = "lm") +
	scale_color_viridis() +
	facet_wrap(~ site)

ggplot(data = phen2[phen2$nGoodData.year > 5, ], mapping = aes(x = year, y = DER.eos - DER.sos, color = nGoodData.year)) +
	geom_point() +
	geom_smooth(method = "lm") +
	scale_color_viridis() +
	facet_wrap(~ site)

ggplot(data = phen2[phen2$nGoodData.year > 5, ], mapping = aes(x = year, y = (DER.eos + DER.sos) / 2, color = nGoodData.year)) +
	geom_point() +
	geom_smooth(method = "lm") +
	scale_color_viridis() +
	facet_wrap(~ site)
