library(rgee)

ee_Initialize()

#####
# HEADER
#####
# Function to mask clouds in sentinel
maskS2clouds <- function(image) {
  qa <- image$select("QA60")

  # Bits 10 and 11 are clouds and cirrus, respectively.
  cloudBitMask <- bitwShiftL(1, 10)
  cirrusBitMask <- bitwShiftL(1, 11)

  # Both flags should be set to zero, indicating clear conditions.
  mask <- qa$bitwiseAnd(cloudBitMask)$eq(0)$
    And(qa$bitwiseAnd(cirrusBitMask)$eq(0))

  return(image$updateMask(mask))
}


#####
# THE PROCESSING
#####

YOI <- 2019  # Year of Interest. Adjust as needed
monthStart <- 7L  # 7 for RMBL, 4 for Luisiana, 6 for matogrosso, 7 for sonora and sierraNevada, 5 for jalisco
monthEnd <- 8L  # 8 for RMBL, 5 for Luisiana, 8 for matogrosso, 8 for sonora and sierraNevada, 10 for jalisco
area <- "northAmerica"  # Adjust as needed: northAmerica, southAmerica, europe, southAfrica, asia, oceania
blockOfInterest <- "13x23"
siteName <- "pinacate"  # A name to the site/region to add to the filename

# Prepare the region and year
YOIint <- ee$Number(YOI)$uint16()
#regions <- ee$FeatureCollection(paste0("users/hinojohinojo/", area, "_sections_2_5_degrees"))
#regionFeature <- ee$Feature(regions$filter(ee$Filter$eq("block", blockOfInterest))$toList(1)$get(0))
#region <- regionFeature$geometry()
#block <- regionFeature$get("block")$getInfo()

# RMBL
region <- ee$Geometry$Polygon(list(
  c(-107.2306, 39.1156),
  c(-106.7309, 39.1156),
  c(-106.7309, 38.6494),
  c(-107.2306, 38.6494),
  c(-107.2306, 39.1156))
)

# luisiana
region <- ee$Geometry$Polygon(list(
  c(-90.6706, 31.3319),
  c(-89.8435, 31.3319),
  c(-89.8435, 30.6858),
  c(-90.6706, 30.6858),
  c(-90.6706, 31.3319))
)

# matogrosso
region <- ee$Geometry$Polygon(list(
  c(-54.3022, -11.0356),
  c(-53.8891, -11.0356),
  c(-53.8891, -11.4900),
  c(-54.3022, -11.4900),
  c(-54.3022, -11.0356))
)

# sonora
region <- ee$Geometry$Polygon(list(
  c(-109.4804, 28.3690),
  c(-108.6596, 28.3690),
  c(-108.6596, 27.7347),
  c(-109.4804, 27.7347),
  c(-109.4804, 28.3690))
)

# pinacate
region <- ee$Geometry$Polygon(list(
  c(-113.988, 32.320),
  c(-112.958, 32.320),
  c(-112.958, 31.369),
  c(-113.988, 31.369),
  c(-113.988, 32.320))
)


# sierraNevada
region <- ee$Geometry$Polygon(list(
  c(-119.1098, 36.6755),
  c(-118.6234, 36.6755),
  c(-118.6234, 36.2740),
  c(-119.1098, 36.2740),
  c(-119.1098, 36.6755))
)

# manitoba
region <- ee$Geometry$Polygon(list(
  c(-98.791, 56.317),
  c(-97.993, 56.317),
  c(-97.993, 55.741),
  c(-98.791, 55.741),
  c(-98.791, 56.317))
)

# grassAsia2
region <- ee$Geometry$Polygon(list(
  c(101.0095, 33.9797),
  c(101.68, 33.9797),
  c(101.68, 33.4272),
  c(101.0095, 33.4272),
  c(101.0095, 33.9797))
)

# jalisco
region <- ee$Geometry$Polygon(list(
  c(-104.732, 19.992),
  c(-104.185, 19.992),
  c(-104.185, 19.503),
  c(-104.732, 19.503),
  c(-104.732, 19.992))
)




# The image collection
sentinel <- ee$ImageCollection("COPERNICUS/S2_SR")$
filterBounds(region)$
filterDate(ee$Date$fromYMD(YOIint, monthStart, 1L), ee$Date$fromYMD(YOIint, monthEnd, 30))$
map(maskS2clouds)$
select("TCI_R", "TCI_G", "TCI_B")$
  median()

# Test the image
Map$addLayer(sentinel)

# Convert to an RGB image
sentinelRGB <- sentinel$visualize(forceRgbOutput = TRUE)

Map$addLayer(sentinelRGB)


taskRGB <- ee_image_to_drive(
  image = sentinelRGB,
  description = paste0("RGB_map_", siteName, "_", area, "_", blockOfInterest , "_", YOI, "_10m"),
  folder = "LMA_regional_maps",
  fileNamePrefix = paste0("RGB_map_", siteName, "_", area, "_", blockOfInterest , "_", YOI, "_10m"),
  timePrefix = FALSE,
  region = region,
  scale = 10L,
  crs = "EPSG:4326",
  maxPixels = 15000000000,
  #shardSize = 1024,
  fileDimensions = 10240L,
  fileFormat = "GeoTIFF"
)

taskRGB$start()
