library(rgee)
library(sf)
library(dplyr)
library(ggplot2)
library(data.table)

ee_Initialize()

#########
# Header
#########

# Function to extract bit data from QA bands
bitwiseExtract <- function(value, fromBit, toBit) {
  maskSize <- ee$Number(1)$add(toBit)$subtract(fromBit)
  mask <- ee$Number(1)$leftShift(maskSize)$subtract(1)
  return(value$rightShift(fromBit)$bitwiseAnd(mask))
}

# Functions to add quality bands
addQA <- function(image) {
  qa <- image$select('pixel_qa')
  cloud <- bitwiseExtract(qa, 5, 5)$rename("CL")
  cloudShadow <- bitwiseExtract(qa, 3, 3)$rename("CS")
  snow <- bitwiseExtract(qa, 4, 4)$rename("SN")
  water <- bitwiseExtract(qa, 2, 2)$rename("WT")
  return(image$addBands(cloud)$addBands(cloudShadow)$addBands(snow)$addBands(water))
}


# Function to harmonize the different Landsats
# according to coefficients in Roy et al 2016 DOI http://dx.doi.org/10.1016/j.rse.2015.12.024
harmonizationRoy <- function(image) {
  slopes <- ee$Image$constant(c(0.8850, 0.9317, 0.9372, 0.8339, 0.8639, 1, 0.9165, 1, 1, 1, 1))  # RMA - create an image of slopes per band for L8 TO L7 regression line - David Roy
  itcp <- ee$Image$constant(c(0.0183, 0.0123, 0.0123, 0.0448, 0.0306, 0, 0.0116, 0, 0, 0, 0))  # RMA - create an image of y-intercepts per band for L8 TO L7 regression line - David Roy
  y <- image$select(c("B2","B3","B4","B5","B6","B10","B7", "CL", "CS", "SN", "WT"), c("B1", "B2", "B3", "B4", "B5", "B6", "B7", "CL", "CS", "SN", "WT"))$  # select OLI bands 2-7 and rename them to match L7 band names
             resample("bicubic")$  # ...resample the L8 bands using bicubic
             multiply(slopes)$add(itcp$multiply(10000))$  # multiply the y-intercept bands by 10000 to match the scale of the L7 bands then apply the line equation
             set("system:time_start", image$get("system:time_start"))  # set the output system:time_start metadata to the input image time_start otherwise it is null
             #set("system:time_end", oli.get("system:time_end"))  # set the output system:time_end metadata to the input image time_end otherwise it is null
  return(y$toShort())  # return the image as short to match the type of the other data
}





#######
# The data
#######

# load the coordinates of the sites
plots <- st_read("field_plots_locations_layers/colorado_plots_v_hinojo.shp")
region <- ee$Geometry$Polygon(list(c(-107.077, 39.005), c(-106.93, 39.005), c(-106.93, 38.87), c(-107.077, 38.87), c(-107.077, 39.005)))  # A region enclosing all the sites

# load the landsat imagery
landsat5 <- ee$ImageCollection("LANDSAT/LT05/C01/T1_SR")$
                  filterDate("1985-01-01", "2012-04-30")$
                  filterBounds(region)$
		  map(addQA)$
                  select("B1","B2","B3","B4","B5","B6","B7","CL","CS","SN","WT")

landsat7 <- ee$ImageCollection("LANDSAT/LE07/C01/T1_SR")$
                  filterDate("2012-05-01", "2013-04-30")$
                  filterBounds(region)$
		  map(addQA)$
                  select("B1","B2","B3","B4","B5","B6","B7","CL","CS","SN","WT")

landsat8 <- ee$ImageCollection("LANDSAT/LC08/C01/T1_SR")$
                  filterDate("2013-05-01", "2020-08-31")$
                  filterBounds(region)$
		  map(addQA)$
                  map(harmonizationRoy)$
                  select("B1","B2","B3","B4","B5","B6","B7","CL","CS","SN","WT")

landsat <- landsat5$merge(landsat7)$merge(landsat8)



startDates <- c("1985-01-01",
		"1986-01-01",
		"1987-01-01",
		"1988-01-01",
		"1989-01-01",
		"1990-01-01",
		"1991-01-01",
		"1992-01-01",
		"1993-01-01",
		"1994-01-01",
		"1995-01-01",
		"1996-01-01",
		"1997-01-01",
		"1998-01-01",
		"1999-01-01",
		"2000-01-01",
		"2001-01-01",
		"2002-01-01",
		"2003-01-01",
		"2004-01-01",
		"2005-01-01",
		"2006-01-01",
		"2007-01-01",
		"2008-01-01",
		"2009-01-01",
		"2010-01-01",
		"2011-01-01",
		"2012-01-01",
		"2013-01-01",
		"2014-01-01",
		"2015-01-01",
		"2016-01-01",
		"2017-01-01",
		"2018-01-01",
		"2019-01-01")

endDates <-   c("1985-12-31",
		"1986-12-31",
		"1987-12-31",
		"1988-12-31",
		"1989-12-31",
		"1990-12-31",
		"1991-12-31",
		"1992-12-31",
		"1993-12-31",
		"1994-12-31",
		"1995-12-31",
		"1996-12-31",
		"1997-12-31",
		"1998-12-31",
		"1999-12-31",
		"2000-12-31",
		"2001-12-31",
		"2002-12-31",
		"2003-12-31",
		"2004-12-31",
		"2005-12-31",
		"2006-12-31",
		"2007-12-31",
		"2008-12-31",
		"2009-12-31",
		"2010-12-31",
		"2011-12-31",
		"2012-12-31",
		"2013-12-31",
		"2014-12-31",
		"2015-12-31",
		"2016-12-31",
		"2017-12-31",
		"2018-12-31",
		"2019-12-31")


# Obtain the landsat data for the RMBL plots
data <- data.frame()
for (i in 1:length(startDates)) {
	
	landsatSub <- landsat$filterDate(startDates[i], endDates[i])
	tmp <- ee_extract(landsatSub, y = plots, fun = ee$Reducer$mean(), scale = 30, sf = TRUE)
	tmp.df <- as.data.frame(st_drop_geometry(tmp))
	tmp.df <- tmp.df[, 14:ncol(tmp.df)]
	rownames(tmp.df) <- tmp$Comment
	tmp.df.t <- as.data.frame(t(tmp.df))
	tmp.datePOSIXlt <- strptime(x = substr(rownames(tmp.df.t), start = nchar(rownames(tmp.df.t)) - 10, stop = nchar(rownames(tmp.df.t)) - 3), format = "%Y%m%d", tz = "GMT")
	tmp.df.t$year <- tmp.datePOSIXlt$year + 1900
	tmp.df.t$jday <- tmp.datePOSIXlt$yday + 1
	tmp.df.t$band <- substr(x = rownames(tmp.df.t), start = nchar(rownames(tmp.df.t)) - 1, stop = nchar(rownames(tmp.df.t)))
	tmp.df.t$pathrow <- substr(x = rownames(tmp.df.t), start = nchar(rownames(tmp.df.t)) - 17, stop = nchar(rownames(tmp.df.t)) - 12)
	tmp.df.long <- melt(data = setDT(tmp.df.t), id.vars = c("year", "jday", "pathrow", "band"), variable.name = "plot", value.name = "value", na.rm = TRUE, variable.factor = FALSE)
	tmp.df.wide <- dcast(data = tmp.df.long, formula = plot + year + jday + pathrow ~ band , drop = TRUE, value.var = "value", fun.aggregate = mean)
	data <- rbind(data, tmp.df.wide)

}

# Fix some of the plot names
#data$plot <- gsub(pattern = "Ba", replacement = "Ba_", x = data$plot, fixed = TRUE)
#data$plot <- gsub(pattern = "PfEnq", replacement = "PfEnq_", x = data$plot, fixed = TRUE)
data$site_plot <- data$plot
data$site <- sub(pattern = "\\_.*", replacement = "", x = data$site_plot)
data$plot <- sub(pattern = ".*\\_", replacement = "", x = data$site_plot)
data <- data[, c("site_plot", "site", "plot", "year", "jday", "pathrow", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "CL", "CS", "SN", "WT")]

# Explore the data before exporting
ggplot(data = data, mapping = aes(x = jday, y = (B4 - B3) / (B4 + B3), color = site)) +
	geom_point() +
	facet_wrap(~ year)

# Export the data
write.csv(x = data, file = "ee_landsat_at_rmbl_plots_allData_explicitQA.csv", row.names = FALSE)




#####
# Forest plots at RMBL
#####

ee_Initialize()

#########
# Header
#########

 # Function to mask clouds and snow based on the pixel_qa band of Landsat 8 SR data.
 # @param {ee.Image} image input Landsat 8 SR image
 # @return {ee.Image} cloudmasked Landsat 8 image

maskl578 <- function(image) {
  # Bits 2, 3, 4 and 5 are water, cloud shadow, cloud, and snow, respectively.
  cloudShadowBitMask <- bitwShiftL(1, 3)
  cloudBitMask <- bitwShiftL(1, 5)
  snowBitMask <- bitwShiftL(1, 4)
  waterBitMask <- bitwShiftL(1, 2)
  # Get the pixel QA band
  pixelQA <- image$select("pixel_qa")
  # Both flags should be set to zero, indicating clear conditions.
  mask <- pixelQA$bitwiseAnd(cloudShadowBitMask)$eq(0)$
                 And(pixelQA$bitwiseAnd(cloudBitMask)$eq(0))$
                 And(pixelQA$bitwiseAnd(snowBitMask)$eq(0))$
                 And(pixelQA$bitwiseAnd(waterBitMask)$eq(0))
  return(image$updateMask(mask))
}




# Function to harmonize the different Landsats
# according to coefficients in Roy et al 2016 DOI http://dx.doi.org/10.1016/j.rse.2015.12.024
harmonizationRoy <- function(image) {
  slopes <- ee$Image$constant(c(0.8850, 0.9317, 0.9372, 0.8339, 0.8639, 1, 0.9165))  # RMA - create an image of slopes per band for L8 TO L7 regression line - David Roy
  itcp <- ee$Image$constant(c(0.0183, 0.0123, 0.0123, 0.0448, 0.0306, 0, 0.0116))  # RMA - create an image of y-intercepts per band for L8 TO L7 regression line - David Roy
  y <- image$select(c("B2","B3","B4","B5","B6","B10","B7"), c("B1", "B2", "B3", "B4", "B5", "B6", "B7"))$  # select OLI bands 2-7 and rename them to match L7 band names
             resample("bicubic")$  # ...resample the L8 bands using bicubic
             multiply(slopes)$add(itcp$multiply(10000))$  # multiply the y-intercept bands by 10000 to match the scale of the L7 bands then apply the line equation
             set("system:time_start", image$get("system:time_start"))  # set the output system:time_start metadata to the input image time_start otherwise it is null
             #set("system:time_end", oli.get("system:time_end"))  # set the output system:time_end metadata to the input image time_end otherwise it is null
  return(y$toShort())  # return the image as short to match the type of the other data
}



#######
# The data
#######

# load the coordinates of the sites and do a buffer around them
plots <- st_read("layers/rmbl_forest_plots/rmbl_forest_plots_landsat.shp")


# Define a region enclosing all the sites
region <- ee$Geometry$Polygon(list(c(-107.05, 39.000), c(-106.79, 39.000), c(-106.79, 38.68), c(-107.05, 38.68), c(-107.05, 39.000)))

# load the landsat imagery
landsat5 <- ee$ImageCollection("LANDSAT/LT05/C01/T1_SR")$
                  filterDate("1985-01-01", "2012-04-30")$
                  filterBounds(region)$
                  map(maskl578)$
                  select("B1","B2","B3","B4","B5","B6","B7")

landsat7 <- ee$ImageCollection("LANDSAT/LE07/C01/T1_SR")$
                  filterDate("2012-05-01", "2013-04-30")$
                  filterBounds(region)$
                  map(maskl578)$
                  select("B1","B2","B3","B4","B5","B6","B7")

landsat8 <- ee$ImageCollection("LANDSAT/LC08/C01/T1_SR")$
                  filterDate("2013-05-01", "2020-08-31")$
                  filterBounds(region)$
                  map(maskl578)$
                  map(harmonizationRoy)$
                  select("B1","B2","B3","B4","B5","B6","B7")

landsat <- landsat5$merge(landsat7)$merge(landsat8)
#                  map(addNDVI_l5)$
#                  map(addNIRv_l5)$
#                  map(addNDMI_l5)$
#                  map(addNDSI_l5)
#                  map(addB1_l5)
#                  map(addB2_l5)
#                  map(addB3_l5)
#                  map(addB4_l5)
#                  map(addB5_l5)
#                  map(addB6_l5)
#                  map(addB7_l5)


startDates <- c("1985-01-01",
		"1986-01-01",
		"1987-01-01",
		"1988-01-01",
		"1989-01-01",
		"1990-01-01",
		"1991-01-01",
		"1992-01-01",
		"1993-01-01",
		"1994-01-01",
		"1995-01-01",
		"1996-01-01",
		"1997-01-01",
		"1998-01-01",
		"1999-01-01",
		"2000-01-01",
		"2001-01-01",
		"2002-01-01",
		"2003-01-01",
		"2004-01-01",
		"2005-01-01",
		"2006-01-01",
		"2007-01-01",
		"2008-01-01",
		"2009-01-01",
		"2010-01-01",
		"2011-01-01",
		"2012-01-01",
		"2013-01-01",
		"2014-01-01",
		"2015-01-01",
		"2016-01-01",
		"2017-01-01",
		"2018-01-01",
		"2019-01-01")

endDates <-   c("1985-12-31",
		"1986-12-31",
		"1987-12-31",
		"1988-12-31",
		"1989-12-31",
		"1990-12-31",
		"1991-12-31",
		"1992-12-31",
		"1993-12-31",
		"1994-12-31",
		"1995-12-31",
		"1996-12-31",
		"1997-12-31",
		"1998-12-31",
		"1999-12-31",
		"2000-12-31",
		"2001-12-31",
		"2002-12-31",
		"2003-12-31",
		"2004-12-31",
		"2005-12-31",
		"2006-12-31",
		"2007-12-31",
		"2008-12-31",
		"2009-12-31",
		"2010-12-31",
		"2011-12-31",
		"2012-12-31",
		"2013-12-31",
		"2014-12-31",
		"2015-12-31",
		"2016-12-31",
		"2017-12-31",
		"2018-12-31",
		"2019-12-31")


# Obtain the landsat data for the RMBL plots
data <- data.frame()
for (i in 1:length(startDates)) {
	
	landsatSub <- landsat$filterDate(startDates[i], endDates[i])
	tmp <- ee_extract(landsatSub, y = plots, fun = ee$Reducer$mean(), scale = 30, sf = TRUE)
	tmp.df <- as.data.frame(st_drop_geometry(tmp))
	tmp.df <- tmp.df[, 4:ncol(tmp.df)]
	rownames(tmp.df) <- tmp$site_pixel
	tmp.df.t <- as.data.frame(t(tmp.df))
	tmp.datePOSIXlt <- strptime(x = substr(rownames(tmp.df.t), start = nchar(rownames(tmp.df.t)) - 10, stop = nchar(rownames(tmp.df.t)) - 3), format = "%Y%m%d", tz = "GMT")
	tmp.df.t$year <- tmp.datePOSIXlt$year + 1900
	tmp.df.t$jday <- tmp.datePOSIXlt$yday + 1
	tmp.df.t$band <- substr(x = rownames(tmp.df.t), start = nchar(rownames(tmp.df.t)) - 1, stop = nchar(rownames(tmp.df.t)))
	tmp.df.long <- melt(data = setDT(tmp.df.t), id.vars = c("year", "jday", "band"), variable.name = "site_pixel", value.name = "value", na.rm = TRUE, variable.factor = FALSE)
	tmp.df.wide <- dcast(data = tmp.df.long, formula = site_pixel + year + jday ~ band , drop = FALSE, value.var = "value")
	data <- rbind(data, tmp.df.wide)

}

# Set columns with site and plot names
data$site <- sub(pattern = "\\:.*", replacement = "", x = data$site_pixel)
data$plot <- as.numeric(sub(pattern = ".*\\:", replacement = "", x = data$site_pixel))
data <- data[, c("site", "plot", "year", "jday", "B1", "B2", "B3", "B4", "B5", "B6", "B7")]

# Explore the data before exporting
ggplot(data = data[data$site == "AlmontCO" & data$year == 2007 & data$B1 > 0, ], mapping = aes(x = jday, y = (B4 - B3) * (B4 / 10000) / (B4 + B3), color = as.factor(plot))) +
	geom_point() +
	coord_cartesian(ylim = c(0, 0.5)) +
	facet_wrap(~ site)

# Export the data
write.csv(x = data, file = "ee_landsat_at_rmbl_forest_plots.csv", row.names = FALSE)

