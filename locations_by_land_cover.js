var america = 
    /* color: #98ff00 */
    /* shown: false */
    ee.Geometry.Polygon(
        [[[-169.07154571996693, 53.330872983017066],
          [-162.53270376149106, 50.4154445996085],
          [-128.69980567721743, 35.54930372087353],
          [-163.85049387359132, 28.73641029111334],
          [-161.7138800719487, 18.28310865847205],
          [-123.01685821996699, 14.68988136661875],
          [-80.47779571996699, -60.93043220292333],
          [-45.32154571996699, -45.36527461487172],
          [-26.33717071996699, -16.33807082383584],
          [-50.243420719967, 50.710716260132465],
          [-112.46998321996699, 69.7648940377966],
          [-168.7199832199669, 70.00669063314739]]]),
    oldworld = 
    /* color: #0b4a8b */
    /* shown: false */
    ee.Geometry.Polygon(
        [[[22.566754633857045, -38.51836831877983],
          [6.260554375208023, -18.359081375628477],
          [6.796337499346108, -4.933831926943102],
          [-5.206682866142964, 0.3896512001110177],
          [-19.620745366142962, 11.904625904777546],
          [-15.753557866142964, 32.57891691439609],
          [-11.690182837609937, 37.40696907994939],
          [-12.724315678684537, 52.327867358647325],
          [-5.558245366142964, 62.932571372816774],
          [4.264360854108431, 64.42352466244617],
          [14.385162926830102, 69.03725821545719],
          [21.76345158008545, 70.91399454777391],
          [32.41050463385704, 71.6479899994799],
          [44.58464118722411, 69.30384786920192],
          [57.58568613573298, 70.12954697550116],
          [68.26987963385704, 73.43928442642515],
          [82.11325256372542, 74.63124230901221],
          [104.5514440866903, 77.91763683490085],
          [128.73862963385704, 74.41240154912877],
          [141.8802588560808, 72.91911045333035],
          [153.34800463385704, 72.6185032449706],
          [-171.14418286614296, 67.55671337937291],
          [-169.3800523349648, 66.12933859412752],
          [-170.28507157381156, 65.68694103039793],
          [-171.40702725393268, 64.28242364572436],
          [-178.70277661614296, 62.28564291925286],
          [166.73674160527335, 57.76987245459604],
          [156.57295560283876, 46.99494801213359],
          [141.3463522972997, 33.87688414493709],
          [128.16210847850925, 22.870177159201347],
          [149.72498746072827, -6.637803364603836],
          [161.98705930104197, -30.420124624340172],
          [-178.38044318966334, -35.03172687436537],
          [-177.82387036614296, -48.43308454008368],
          [132.70236636847042, -39.33159853518992],
          [109.75425463385703, -34.85467866811394],
          [111.05381981489583, -14.908054366962643],
          [90.23725177104828, 3.790693308226178],
          [73.34375128156667, 5.686399651898281],
          [64.21300232391854, 19.587100483841255],
          [50.68073934786201, 0.03472147779008835],
          [60.874519261635164, -22.405519001932767],
          [40.564415920161146, -28.8641197762019]]]);


// To export sample locations per land cover
// Based on the original datasets


// This is the main dataset
var vegClasses = ee.ImageCollection('COPERNICUS/Landcover/100m/Proba-V-C3/Global')
                .filterDate('2019-01-01', '2019-12-31')
                .select('discrete_classification')
                .first();

// Prepare some layers to use as mask for quality control of land classes
// A mask for the certainty of the classes
var vegQaMask = ee.ImageCollection('COPERNICUS/Landcover/100m/Proba-V-C3/Global')
                .filterDate('2019-01-01', '2019-12-31')
                .select('discrete_classification-proba')
                .first()
                .gte(50);  // keep only pixels with more than 50% certainty

// A mask areas with very low vegetation according to the amount of bare soil
var bareSoilMask = ee.ImageCollection('COPERNICUS/Landcover/100m/Proba-V-C3/Global')
                .filterDate('2015-01-01', '2019-12-31')
                .select('bare-coverfraction')
                .mean()
                .lte(50);  // Keep only pixels with less than 50% bare soil

// A mask for water bodies
var water = ee.Image('JRC/GSW1_3/GlobalSurfaceWater');
var waterMask = water.select('occurrence').eq(0)
  .and(water.select('recurrence').eq(0))
  .unmask(1, true);
  
// Masks for latitudinal bands
var lat = ee.Image.pixelLonLat().select('latitude');
// for tropics
var tropicsMask = lat.gt(-23.43642)
  .and(lat.lt(23.43642));

// for mid latitude
var midMask = lat.gt(-66.56083).and(lat.lt(-23.43642))
  .or(lat.gt(23.43642).and(lat.lt(66.56083)))

// A mask using LMAslope
var LMAmask = ee.Image('users/hinojohinojo/global_LMA_files/LMAslope_map')
  .select('b1').divide(ee.Image('users/hinojohinojo/global_LMA_files/LMAslope_map')
  .select('b1'));

// A mask of human footprint
var humanMask = ee.Image('users/hinojohinojo/human_footprint')
	.lte(4);
      

// Keep only a few classes (closed forests and herbaceous)
var classesMask = vegClasses.eq(30)
  .or(vegClasses.eq(111))
  .or(vegClasses.eq(112))
  .or(vegClasses.eq(113))
  .or(vegClasses.eq(114))
  .or(vegClasses.eq(115))
  .or(vegClasses.eq(20));

// Apply the masks, uncomment/comment as needed
vegClasses = vegClasses
  .updateMask(vegQaMask)
  .updateMask(bareSoilMask)
  .updateMask(classesMask)
  .updateMask(waterMask)
  // .updateMask(tropicsMask)
  // .updateMask(midMask)
  .updateMask(LMAmask)
  // .updateMask(humanMask)
  ;

// Randomly sample locations at all classes
var locations = vegClasses.stratifiedSample({
  // numPoints: 250,  // For latitude bands (250)
  // numPoints: 500,  // For whole world (500)
  numPoints: 50,  // For less locations (100)
  classBand: 'discrete_classification',
  region: america,  // Choose america or oldworld
  // projection: 'EPSG:4326',
  scale: 300,  // Runs with 300 and above for the old world
  seed: 12345,
  dropNulls: true,
  geometries:true
});

Map.addLayer({eeObject: vegClasses.select('discrete_classification'), name: 'vegClasses'});
// Map.addLayer(LMAmask);
// Map.addLayer({eeObject: tropicsMask, name: 'tropicsMask'});
// Map.addLayer({eeObject: midMask, name: 'midMask'});
Map.addLayer({eeObject: locations, visParams: {color: '#FF0000'}, name: 'locations'});

// Export.table.toAsset({
//   collection: locations,
//   description: 'land_cover_locations',
//   assetId: 'land_cover_locations_america'  // change america or oldworld as needed
// });


// Adjust the name of the file to export
var fileName = 'land_cover_locations_america_smallSample';

Export.table.toDrive({
  collection: locations,
  description: fileName,
  folder: 'world_land_cover',
  fileNamePrefix: fileName,  // change america or oldworld as needed
  fileFormat: 'SHP'
});
