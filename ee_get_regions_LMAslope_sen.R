library(rgee)
library(reticulate)

ee_Initialize(user = "chinojoh@email.arizona.edu")


#########
# HEADER
#########

#Function to mask clouds and snow based on the pixel_qa band of Landsat 8 SR data.
maskl578 <- function(image) {
  # Bits 2, 3, 4 and 5 are water, cloud shadow, cloud, and snow, respectively.
  cloudShadowBitMask <- bitwShiftL(1, 3)
  cloudBitMask <- bitwShiftL(1, 5)
  snowBitMask <- bitwShiftL(1, 4)
  waterBitMask <- bitwShiftL(1, 2)
  # Get the pixel QA band
  pixelQA <- image$select("pixel_qa")
  # Both flags should be set to zero, indicating clear conditions.
  mask <- pixelQA$bitwiseAnd(cloudShadowBitMask)$eq(0)$
                 And(pixelQA$bitwiseAnd(cloudBitMask)$eq(0))$
                 And(pixelQA$bitwiseAnd(snowBitMask)$eq(0))$
                 And(pixelQA$bitwiseAnd(waterBitMask)$eq(0))
  return(image$updateMask(mask))
}

# Function to harmonize the different Landsats
# according to coefficients in Roy et al 2016 DOI http://dx.doi.org/10.1016/j.rse.2015.12.024
harmonizationRoy <- function(image) {
  slopes <- ee$Image$constant(c(0.8850, 0.9317, 0.9372, 0.8339, 0.8639, 1, 0.9165))  # RMA - create an image of slopes per band for L8 TO L7 regression line - David Roy
  itcp <- ee$Image$constant(c(0.0183, 0.0123, 0.0123, 0.0448, 0.0306, 0, 0.0116))  # RMA - create an image of y-intercepts per band for L8 TO L7 regression line - David Roy
  y <- image$select(c("B2","B3","B4","B5","B6","B10","B7"), c("B1", "B2", "B3", "B4", "B5", "B6", "B7"))$  # select OLI bands 2-7 and rename them to match L7 band names
    resample("bicubic")$  # ...resample the L8 bands using bicubic
    multiply(slopes)$add(itcp$multiply(10000))$  # multiply the y-intercept bands by 10000 to match the scale of the L7 bands then apply the line equation
    set("system:time_start", image$get("system:time_start"))  # set the output system:time_start metadata to the input image time_start otherwise it is null
    #set("system:time_end", oli.get("system:time_end"))  # set the output system:time_end metadata to the input image time_end otherwise it is null
  return(y$toShort())  # return the image as short to match the type of the other data
}




# Functions for vegetation indices
addNDVI_l5 <- function(image) {
  ndvi <- image$normalizedDifference(c("B4", "B3"))$rename("NDVI")
  return(image$addBands(ndvi))
}

addNIRv_l5 <- function(image) {
  NIRv <- image$select("NDVI")$multiply(image$select("B4")$divide(10000))$rename("NIRv")
  return(image$addBands(NIRv))
}

addNDMI_l5 <- function(image) {
  ndmi <- image$normalizedDifference(c("B4", "B5"))$rename("NDMI")
  return(image$addBands(ndmi))
}

# Function to calculate iLMA
addiLMA <- function(image) {
  iLMA <- image$expression('iLMA = (b("NDMI") + 0.51) / b("NIRv")')
  return(image$addBands(iLMA))
}

# Function to calculate LMA from iLMA
addLMA <- function(image) {
  LMA <- image$expression('LMA = -0.0045886 + (0.0043203 * b("iLMA"))')
  return(image$addBands(LMA))
}

# Function to add DOY from an image collection
addDate <- function(image) {
  doy <- image$date()$getRelative("day", "year")
  doyBand <- ee$Image$constant(doy)$add(1)$uint16()$rename("DOY")
  doyBand <- doyBand$updateMask(image$select("NDMI")$mask())
  
  return(image$addBands(doyBand))
}





#########
# PROCESSING
#########
# Adjust area and block as needed
area <- "northAmerica"
blockOfInterest <- "09x27"
siteName <- "jalisco2"  # A name to the site/region to add to the filename

# Regions asset
regions <- ee$FeatureCollection(paste0("users/hinojohinojo/", area, "_sections_2_5_degrees"))
regionFeature <- ee$Feature(regions$filter(ee$Filter$eq("block", blockOfInterest))$toList(1)$get(0))
region <- regionFeature$geometry()
block <- regionFeature$get("block")$getInfo()

# Comment the "Regions asset" part and uncoment any of these lines to do other specific region
#region <- ee$Geometry$Polygon(list(c(-107.2601923969994857, 39.1104016819999458), c(-106.6999131547883479, 39.1104016819999458), c(-106.6999131547883479, 38.6298928371256238), c(-107.2601923969994857, 38.6298928371256238), c(-107.2601923969994857, 39.1104016819999458)))  # The region around RMBL for Nicola

# Prepare the mask to mask bare areas
landMask <- ee$ImageCollection("COPERNICUS/Landcover/100m/Proba-V-C3/Global")$
  select("bare-coverfraction")$
  mean()
landMask <- landMask$lte(50)

# Water layers
# for masking water bodies from LMA map
water <- ee$Image("JRC/GSW1_3/GlobalSurfaceWater")
waterMask = water$select("occurrence")$eq(0)$
  And(water$select("recurrence")$eq(0))$
  unmask(1, TRUE)



# What years have data
years5 <- ee$List$sequence(1985, 2011, 1)$
  map(ee_utils_pyfunc(function(item) {
    YOI <- ee$Number(item)$uint16()
    YOIst <- ee$String(YOI)
    feat <- ee$Feature(NULL, list(year = YOI))
    yearCol <- ee$ImageCollection("LANDSAT/LT05/C01/T1_SR")$
      filterBounds(region)$
      filterDate(ee$Date$fromYMD(YOI, 1L, 1L), ee$Date$fromYMD(YOI, 12L, 31L))
    size <- ee$Number(yearCol$size())$uint16()
    return(feat$set(list(n = size)))
  }))

years7 <- ee$List(list(2012))$
  map(ee_utils_pyfunc(function(item) {
    YOI <- ee$Number(item)$uint16()
    feat <- ee$Feature(NULL, list(year = YOI))
    yearCol <- ee$ImageCollection("LANDSAT/LE07/C01/T1_SR")$
      filterBounds(region)$
      filterDate(ee$Date$fromYMD(YOI, 1L, 1L), ee$Date$fromYMD(YOI, 12L, 31L))
    size <- ee$Number(yearCol$size())$uint16()
    return(feat$set(list(n = size)))
  }))

years8 <- ee$List$sequence(2013, 2019, 1)$
  map(ee_utils_pyfunc(function(item) {
    YOI <- ee$Number(item)$uint16()
    feat <- ee$Feature(NULL, list(year = YOI))
    yearCol <- ee$ImageCollection("LANDSAT/LC08/C01/T1_SR")$
      filterBounds(region)$
      filterDate(ee$Date$fromYMD(YOI, 1L, 1L), ee$Date$fromYMD(YOI, 12L, 31L))
      size <- ee$Number(yearCol$size())$uint16()
    return(feat$set(list(n = size)))
}))

years <- years5$cat(years7)$cat(years8)
years <- ee$FeatureCollection(years)

# Keep only years with more than that amount of images
yearsWithData <- years$filter(ee$Filter$gte("n", 3));

# The processing of the images
LMAsCollection <- yearsWithData$map(function(feat){
  #feat <- ee$Feature(yearsWithData$toList(yearsWithData$size())$get(2))  # For testing
  YOIint <- ee$Number(feat$get("year"))$uint16()  # Year Of Interest
  YOIst <- ee$String(YOIint)
      
  # Load the correct landsat data for each year/step
  landsat <- ee$Algorithms$If(YOIint$lte(2011),
    ee$ImageCollection("LANDSAT/LT05/C01/T1_SR")$
filterBounds(region)$
      filterDate(ee$Date$fromYMD(YOIint, 1L, 1L), ee$Date$fromYMD(YOIint, 12L, 31L))$
      map(maskl578)$
      select("B3","B4","B5"),
          
    ee$Algorithms$If(YOIint$eq(2012),
ee$ImageCollection("LANDSAT/LT05/C01/T1_SR")$
  filterBounds(region)$
  filterDate(ee$Date$fromYMD(YOIint, 1L, 1L), ee$Date$fromYMD(YOIint, 4L, 30L))$
  map(maskl578)$
  select("B3","B4","B5")$
  merge(
    ee$ImageCollection("LANDSAT/LE07/C01/T1_SR")$
      filterBounds(region)$
      filterDate(ee$Date$fromYMD(YOIint, 5L, 1L), ee$Date$fromYMD(YOIint, 12L, 31L))$
      map(maskl578)$
      select("B3","B4","B5")
  ),
ee$Algorithms$If(YOIint$gte(2013),
  ee$ImageCollection("LANDSAT/LC08/C01/T1_SR")$
    filterDate(ee$Date$fromYMD(YOIint, 1L, 1L), ee$Date$fromYMD(YOIint, 12L, 31L))$
    filterBounds(region)$
    map(maskl578)$
    map(harmonizationRoy)$
    select("B3","B4","B5")
)
    )
  )
  
  landsat <- ee$ImageCollection(landsat)$
    map(addNDVI_l5)$
    map(addNIRv_l5)$
    map(addNDMI_l5)$
    map(addiLMA)$
    map(addLMA)$
    select("LMA", "NDMI", "NIRv")
    
  # Here is the moving average thing
  # I used the moving average to make the map with the mean LMA
  # around (two months) the peak NIRv value
  join <- ee$Join$saveAll(
    matchesKey = "images"
  )
  
  timeField <- "system:time_start"
  timeDiff <- 1000 * 60 * 60 * 24 * 30  # This makes a window of 2 months around each date/image

  
  diffFilter <- ee$Filter$maxDifference(
    difference = timeDiff,
    leftField = timeField,
    rightField = timeField
  )
  
  threeNeighborJoin = join$apply(
    primary = landsat, 
    secondary = landsat, 
    condition = diffFilter
  )
  
  smoothed <- ee$ImageCollection(threeNeighborJoin$map(function(image) {
    landsat <- ee$ImageCollection$fromImages(image$get("images"))
    return(ee$Image(image)$addBands(landsat$median()))
  }))
  # Here it ends the moving average thing
  
  # The following steps add Day of year of images as a band
  # This band is the basis for making the map
  smoothed <- smoothed$map(addDate)
  
  # Here we get the date of the maximum NIRv
  # and make images of the 2 month window around that date
  centerDOY <- smoothed$qualityMosaic("NIRv")$select("DOY")
  startDOY <- centerDOY$subtract(30)
  endDOY <- centerDOY$add(30)

  # We mask all data outside the window
  # and make and average of LMA and NDMI within the window
  
  
  # A function to mask images outside a window of dates
  smoothed <- smoothed$map(function(image) {
    doy <- image$select("DOY")
    mask <- doy$gte(startDOY)$And(doy$lte(endDOY))
    return(image$updateMask(mask))
  })
  
  LMAyear <- smoothed$select("LMA_1")$mean()
  NDMIyear <- smoothed$select("NDMI_1")$mean()

  # Mask all LMA taken at low LAI, which is done with using a value
  # as an NDMI threshold (0-0.1)
  NDMImask <- NDMIyear$gte(0)
  LMAyear <- LMAyear$updateMask(NDMImask)

  # Also mask all bare areas
  LMAyear <- LMAyear$updateMask(landMask)

  # And the water areas
  LMAyear <- LMAyear$updateMask(waterMask)

  # Convert LMA from g cm-2 to g m-2 units
  LMAyear <- LMAyear$multiply(10000)

  # Apply the correction to cross-calibrate Landsats
  # Comment out if these lines if the correction is not needed
  LMAyear <- ee$Algorithms$If(YOIint$lt(2012),  # Correction for Landsat 5
    LMAyear$multiply(0.858488)$add(13.007041),

    ee$Algorithms$If(YOIint$gt(2012),  # Correction for Landsat 8
      LMAyear$multiply(1.05687)$add(-2.03858),

      LMAyear$multiply(1)))  # No Correction for Landsat 7
      
  # Convert LMA image to unsigned integer, to reduce the resulting
  # image filesize
  # also adds the date as metadata, year as band and a constant band
  # year and constant bands are used in the regression step
  LMAyear <- ee$Image(LMAyear)$toUint16()$
    set("system:time_start", ee$Date$fromYMD(YOIint, 1L, 1L)$millis())$
    addBands(ee$Image$constant(YOIint)$toUint16()$rename("year"))$
    addBands(ee$Image$constant(1)$toUint16()$rename("constant"))
      

  return(LMAyear)


})

# Theil Sen slope estimate
LMAsen <- ee$ImageCollection(LMAsCollection)$
  select("year", "LMA_1")$
  reduce(reducer = ee$Reducer$sensSlope(), parallelScale = 16)$
  select("slope")$
  rename("LMAslope")$
  multiply(100)$toShort()$
  unmask(-32768, TRUE)  # No-data value

#Map$addLayer(LMAyear, visParams = list(bands = "LMA_1", min = -7000, max = 7000, palette = c("#FFFF00", "#006400")))
#Map$addLayer(LMAsen, visParams = list(bands = "LMAslope", min = -7, max = 7, palette = c("#8B0000", "#FFFFFF", "#00008B")))

# Cast the output into an Image Collection with only LMA to use in the Kendall analysis
LMAsCollection <- ee$ImageCollection(LMAsCollection)$select("LMA_1")



##### Man Kendall test and significance from the Earth Engine Tutorial #####
afterFilter <- ee$Filter$lessThan(
  leftField = "system:time_start",
  rightField = "system:time_start"
)

joined <- ee$ImageCollection(ee$Join$saveAll("after")$apply(
  primary = LMAsCollection,
  secondary = LMAsCollection,
  condition = afterFilter
))

sign <- function(i, j) {  # i and j are images
  return(ee$Image(j)$neq(i)$  # Zero case
    multiply(ee$Image(j)$subtract(i)$clamp(-1, 1))$int()
  )
}

kendall <- ee$ImageCollection(joined$map(function(current) {
  afterCollection <- ee$ImageCollection$fromImages(current$get("after"))
  return(afterCollection$map(function(image) {
    # The unmask is to prevent accumulation of masked pixels that
    # result from the undefined case of when either current or image
    # is masked.  It won't affect the sum, since it's unmasked to zero.
    return(ee$Image(sign(current, image))$unmask(0))
  }))
  # Set parallelScale to avoid User memory limit exceeded.
})$flatten())$reduce("sum", 16)

#ee_print(kendall)

#paletteKendall <- c("red", "white", "green")
# Stretch this as necessary.
#Map$addLayer(kendall, visParams = list(min = -1000, max = 1000, palette = paletteKendall), "kendall")


# Kendall variance
# Values that are in a group (ties).  Set all else to zero.
groups <- LMAsCollection$map(function(i) {
  matches <- LMAsCollection$map(function(j) {
    return(i$eq(j)) # i and j are images.
  })$sum()
  return(i$multiply(matches$gt(1)))
})

# Compute tie group sizes in a sequence.  The first group is discarded.
group <- function(array) {
  length <- array$arrayLength(0)
  # Array of indices.  These are 1-indexed.
  indices <- ee$Image(1)$
    arrayRepeat(0, length)$
    arrayAccum(0, ee$Reducer$sum())$
    toArray(1)
  sorted <- array$arraySort()
  left <- sorted$arraySlice(0, 1)
  right <- sorted$arraySlice(0, 0, -1)
  # Indices of the end of runs.
  mask <- left$neq(right)$
  # Always keep the last index, the end of the sequence.
    arrayCat(ee$Image(ee$Array(list(list(1)))), 0)  # Not sure if here is an error in translation to rgee, originally (in javascript) was arrayCat(ee$Image(ee$Array([[1]])), 0);
  runIndices <- indices$arrayMask(mask)
  # Subtract the indices to get run lengths.
  groupSizes <- runIndices$arraySlice(0, 1)$
    subtract(runIndices$arraySlice(0, 0, -1))
  return(groupSizes)
}

# See equation 2.6 in Sen (1968).
factors <- function(image) {
  return(image$expression("b() * (b() - 1) * (b() * 2 + 5)"))
}

groupSizes <- group(groups$toArray())
groupFactors <- factors(groupSizes)
groupFactorSum <- groupFactors$arrayReduce("sum", list(0))$
  arrayGet(list(0, 0))

count <- joined$count()

kendallVariance <- factors(count)$
  #subtract(groupFactorSum)$
  divide(18)$
  float()
#Map$addLayer(kendallVariance, visParams = list(min = -10000, max = 10000), name = "kendallVariance")

# Significance testing
# Compute Z-statistics.
zero <- kendall$multiply(kendall$eq(0))
pos <- kendall$multiply(kendall$gt(0))$subtract(1)
neg <- kendall$multiply(kendall$lt(0))$add(1)

z <- zero$
  add(pos$divide(kendallVariance$sqrt()))$
  add(neg$divide(kendallVariance$sqrt()))
# Map.addLayer(z, {min: -2, max: 2}, 'z');

# https://en.wikipedia.org/wiki/Error_function#Cumulative_distribution_function
eeCdf <- function(z) {
  return(ee$Image(0.5)$
    multiply(ee$Image(1)$add(ee$Image(z)$divide(ee$Image(2)$sqrt())$erf()))
  )
}

invCdf <- function(p) {
  return(ee$Image(2)$sqrt()$
    multiply(ee$Image(p)$multiply(2)$subtract(1)$erfInv())
  )
}

# Compute P-values.
p <- ee$Image(1)$subtract(eeCdf(z$abs()))$
  multiply(10000)$
  toShort()$
  unmask(-32768, TRUE)  # No-data value
#ee_print(p)
#Map$addLayer(p, visParams = list(min = 0, max = 1), name = "p")

# Pixels that can have the null hypothesis (there is no trend) rejected.
# Specifically, if the true trend is zero, there would be less than 5%
# chance of randomly obtaining the observed result (that there is a trend).
#Map.addLayer(p.lte(0.025), {min: 0, max: 1}, 'significant trends');
  

# Export the images at 30 m resolution
slopeTask <- ee_image_to_drive(
  image = LMAsen,
  description = paste0("LMAsenSlope_map_", siteName, "_", area, "_", blockOfInterest, "_30m"),
  folder = "LMA_regional_maps",
  fileNamePrefix = paste0("LMAsenSlope_map_", siteName, "_", area, "_", blockOfInterest, "_30m"),
  timePrefix = FALSE,
  region = region,
  scale = 30L,
  crs = "EPSG:4326",
  maxPixels = 15000000000,
  #shardSize = 1024,
  fileDimensions = 10240L,
  fileFormat = "GeoTIFF"
)
slopeTask$start()

pTask <- ee_image_to_drive(
  image = p,
  description = paste0("pvalue_map_", siteName, "_", area, "_", blockOfInterest, "_30m"),
  folder = "LMA_regional_maps",
  fileNamePrefix = paste0("pvalue_map_", siteName, "_", area, "_", blockOfInterest, "_30m"),
  timePrefix = FALSE,
  region = region,
  scale = 30L,
  crs = "EPSG:4326",
  maxPixels = 15000000000,
  #shardSize = 1024,
  fileDimensions = 10240L,
  fileFormat = "GeoTIFF"
)
pTask$start()


# Failed areas
#northAmerica 16x25
