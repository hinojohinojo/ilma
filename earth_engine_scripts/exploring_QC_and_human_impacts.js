var region = 
    /* color: #d63000 */
    /* shown: false */
    ee.Geometry.Polygon(
        [[[-170.8054441654845, 59.67256615354733],
          [-167.75699343403835, 63.12444699330752],
          [-169.3991941654845, 68.75577777945934],
          [-151.8210691654845, 72.26522112584334],
          [-133.5398191654845, 70.17344209236731],
          [-128.3668695915073, 70.74249239792974],
          [-119.1257566654845, 69.99380453699892],
          [-115.17387345370504, 68.9445740453455],
          [-111.61985141573605, 68.07526879880018],
          [-106.7301324878077, 69.01149493731087],
          [-101.02028791548435, 68.14240963848385],
          [-98.02887577268109, 69.97867262944125],
          [-82.78298322798435, 69.82776636087881],
          [-79.66972905995323, 68.78343640674383],
          [-78.5420580506215, 64.14592998301984],
          [-61.1179441654845, 59.84961491939881],
          [-44.2429441654845, 55.92993951489615],
          [-5.922631665484506, 60.02572679078852],
          [23.257055834515494, 71.38819217222006],
          [32.04611833451549, 71.38819217222006],
          [41.53830583451549, 69.2595336197146],
          [53.796226428830316, 70.02838048268589],
          [58.7391188503717, 70.59129446203625],
          [63.68674333451549, 70.58659401933943],
          [68.9601808345155, 73.80298394344086],
          [104.90744645951565, 77.78821201084762],
          [119.58518083451548, 76.41621759005494],
          [118.17893083451548, 74.66254417256272],
          [132.24143083451548, 73.9007607722116],
          [135.75705583451548, 77.06125573671632],
          [160.28330412820782, 77.10053583268643],
          [156.14768083451548, 74.84742672815219],
          [159.15095054498394, 71.85819565924614],
          [-176.5969966319951, 71.71455382879637],
          [-172.91481916548452, 67.9782173542937],
          [-169.75075666548452, 66.55206624089212],
          [-168.20085755236718, 62.81452364393511],
          [-173.44216291548452, 62.47614004846054],
          [174.95627458451548, 60.461929272914006],
          [162.82736833451548, 51.35030732838795],
          [135.40549333451548, 27.302181024312347],
          [128.02268083451548, 17.233885415705853],
          [131.88986833451548, 7.2847713192719565],
          [156.49924333451565, -3.240667870624536],
          [-174.83119822525384, -14.009993893607566],
          [178.29611833451565, -48.69096039092549],
          [148.76486833451565, -47.27922900257082],
          [126.96799333451548, -37.71859032558814],
          [108.68674333451548, -36.597889133070204],
          [104.46799333451548, -11.178401873711772],
          [87.5929933345155, 1.7575368113083254],
          [74.5851808345155, 2.4601811810210052],
          [64.0383058345155, 13.923403897723347],
          [53.13986833451549, 3.8642546157214084],
          [55.24924333451549, -15.961329081596647],
          [47.86643083451549, -30.751277776257812],
          [23.608618334515494, -37.439974052270564],
          [10.249243334515494, -34.016241889667015],
          [2.5148683345154943, -0.7031073524364783],
          [-16.821069165484506, 3.162455530237848],
          [-32.9929441654845, -1.7575368113083125],
          [-34.2234129154845, -12.897489183755892],
          [-39.1452879154845, -23.563987128451217],
          [-55.8445066654845, -37.996162679728116],
          [-64.2820066654845, -49.03786794532642],
          [-58.3054441654845, -50.17689812200105],
          [-53.7351316654845, -52.58970076871779],
          [-65.3366941654845, -56.6562264935022],
          [-74.4773191654845, -56.072035471800866],
          [-80.4538816654845, -49.95121990866204],
          [-75.8835691654845, -18.47960905583197],
          [-85.3757566654845, -5.441022303717961],
          [-93.4616941654845, 0.17578097424708533],
          [-91.7038816654845, 11.350796722383672],
          [-109.6335691654845, 17.14079039331665],
          [-130.82089184302578, 35.71015549422054],
          [-145.4395328689418, 21.251724988635942],
          [-151.15737362389638, 15.278003453521686],
          [-162.7195066654845, 17.811456088564523],
          [-162.19429851063066, 23.83737996684216],
          [-149.4573153477156, 23.380169490440554],
          [-130.46774343603332, 44.27960394676777],
          [-138.90524343603332, 57.09074087203263],
          [-165.62399343603332, 50.46011176105434],
          [179.32677034388706, 50.54220125865511],
          [169.24831356296252, 52.784613034720735],
          [-174.06149343603332, 54.31891248123546]]]);



// Define some color palletes to use in the maps
var palettes = require('users/gena/packages:palettes'),
    sequential = palettes.crameri.batlow[25].reverse(),  // Batlow palette
    divergent = palettes.cmocean.Balance[7].reverse();  // Divergen palette
    

// Read the data
var yearsWithData = ee.Image('users/hinojohinojo/global_LMA_files/QCs_yearsWithData');
var rangeOfYears = ee.Image('users/hinojohinojo/global_LMA_files/QCs_rangeOfYears');
var DOYvariance = ee.Image('users/hinojohinojo/global_LMA_files/QCs_DOYvariance');
var NDMImean = ee.Image('users/hinojohinojo/global_LMA_files/QCs_NDMImean');
var nImagesAll = ee.Image('users/hinojohinojo/global_LMA_files/QCs_nImagesAll');
var nImagesAllSd = ee.Image('users/hinojohinojo/global_LMA_files/QCs_nImagesAllSd');

// Rescaling the data from 0 to 10000
// Following equations from here https://support.esri.com/en/technical-article/000008671
var yearsWithDataRsc = yearsWithData.expression(
  'yearsWithDataRsc = ((b("b1") - 0) * (10000 - 0) / (35 - 0)) + 0');
var rangeOfYearsRsc = rangeOfYears.expression(
  'rangeOfYearsRsc = ((b("b1") - 0) * (10000 - 0) / (35 - 0)) + 0');
var DOYvarianceRsc = DOYvariance.multiply(-1).add(10000);

var NDMImeanRsc = NDMImean.where(NDMImean.gt(1000), 1000);
var NDMImeanRsc = NDMImeanRsc.where(NDMImeanRsc.lt(-500), -500);
var NDMImeanRsc = NDMImeanRsc.expression(
  'NDMImeanRsc = ((b("b1") + 500) * (10000 - 0) / (1000 + 500)) - 500');
  
// New nImages Layers
var nImagesAllLow = nImagesAll.subtract(nImagesAllSd);

// Mask layers
var yearsWithDataMask = yearsWithData.gte(10);
var rangeOfYearsMask = rangeOfYears.gte(25)
var nImagesAllLowMask = nImagesAllLow.gte(3);
var NDMImeanMask = NDMImean.gte(0);




// Composing the QCs
var QCtot = yearsWithDataRsc.toInt32()
  .add(rangeOfYearsRsc.toInt32())
  .add(DOYvarianceRsc.toInt32())
  .add(NDMImeanRsc.toInt32());



Map.addLayer({
  eeObject: yearsWithData,
  visParams: {min: 0, max: 35},
  name: 'yearsWithDataRsc'
});
Map.addLayer({
  eeObject: rangeOfYears,
  visParams: {min: 0, max: 35},
  name: 'rangeOfYearsRsc'
});
Map.addLayer({
  eeObject: DOYvariance,
  visParams: {min: 0, max: 10000},
  name: 'DOYvariance'
});
Map.addLayer({
  eeObject: NDMImean,
  visParams: {min: 0, max: 10000},
  name: 'NDMImeanRsc'
});
// Map.addLayer({
//   eeObject: QCtot,
//   visParams: {min: 0, max: 40000},
//   name: 'QCtot'
// });
Map.addLayer({
  eeObject: nImagesAll,
  visParams: {min: 0, max: 50, palette: sequential},
  name: 'nImagesAll'
});
Map.addLayer({
  eeObject: nImagesAllSd,
  visParams: {min: 0, max: 20, palette: sequential},
  name: 'nImagesAllSd'
});
Map.addLayer({
  eeObject: nImagesAllLow,
  visParams: {min: 0, max: 1},
  name: 'nImagesAllLow'
});
Map.addLayer({
  eeObject: nImagesAllLow.gte(4),
  visParams: {min: 0, max: 1},
  name: 'nImagesAllLow'
});





// Mask LMA slope sen and LMA
var LMAslope = ee.Image('users/hinojohinojo/global_LMA_files/LMAslopeSen_map');
// var QCmask = QCtot.gt(30000);  // Old version
// var QCmask = yearsWithData.gt(25)
// var LMAslope = LMAslope.updateMask(QCmask);  // Old version
var LMAslope = LMAslope
  .updateMask(yearsWithDataMask)
  .updateMask(rangeOfYearsMask)
  .updateMask(nImagesAllLowMask)
  .updateMask(NDMImeanMask)
;

// The LMA of 2019
var LMA = ee.Image('users/hinojohinojo/global_LMA_files/LMA_map_2019');
var LMA = LMA
  .updateMask(yearsWithDataMask)
  .updateMask(rangeOfYearsMask)
  .updateMask(nImagesAllLowMask)
  .updateMask(NDMImeanMask)
;




Map.addLayer({
  eeObject: LMAslope,
  visParams: {min: -300, max: 300, palette: divergent},
  name: 'LMAslope'
});

Map.addLayer({
  eeObject: LMA,
  visParams: {min: 50, max: 300, palette: sequential},
  name: 'LMA'
});




///// Global statistics calculation /////
// Water layers
// for masking water bodies from LMA map
var water = ee.Image("JRC/GSW1_3/GlobalSurfaceWater");
var waterMask = water.select('occurrence').eq(0)
  .and(water.select('recurrence').eq(0))
  .unmask(1, true);
  
// Apply the water masks
LMA = LMA.updateMask(waterMask);
LMAslope = LMAslope.updateMask(waterMask);

// Mollweide projection. Fairly good projection for global map
var proj = ' \
  PROJCS["World_Mollweide", \
    GEOGCS["GCS_WGS_1984", \
      DATUM["WGS_1984", \
        SPHEROID["WGS_1984",6378137,298.257223563]], \
      PRIMEM["Greenwich",0], \
      UNIT["Degree",0.017453292519943295]], \
    PROJECTION["Mollweide"], \
    PARAMETER["False_Easting",0], \
    PARAMETER["False_Northing",0], \
    PARAMETER["Central_Meridian",0], \
    UNIT["Meter",1], \
    AUTHORITY["EPSG","54009"]]';

// Get an average global LMA and LMAslope
var LMAmean = LMA.reduceRegion({
  reducer: ee.Reducer.mean(),
  scale: 1000,
  geometry: region,
  crs: proj,
  maxPixels: 500000000
});

var LMAslopeMean = LMAslope.reduceRegion({
  reducer: ee.Reducer.mean(),
  scale: 1000,
  geometry: region,
  crs: proj,
  maxPixels: 500000000
});
var LMAslopeCount = LMAslope.reduceRegion({
  reducer: ee.Reducer.count(),
  scale: 1000,
  geometry: region,
  crs: proj,
  maxPixels: 500000000
});

print("LMAmean", LMAmean);
print("LMAslopeMean", LMAslopeMean);
print("LMAslopeCount", LMAslopeCount);


///// Statistics of areas with high human footprint
// Uncomment these lines for human footprint index
// var human = ee.Image('users/hinojohinojo/human_footprint');  // Human footprint dataset
// var humanMask = human.gt(0.4);  // Change this threshold if needed

// Uncomment these lines for Human modification index 2015
// var human = ee.Image('users/hinojohinojo/global_human_modification_index');  // Human modification index
// var humanMask = human.gt(0.4);  // Change this threshold if needed

// Uncomment these for checking areas where human modification index
// increased from 1990 to 2015
var hmi1990 = ee.Image("users/hinojohinojo/human_modification_index/gHMv1_4_300m_1990_cland")
  .divide(65536);
var hmi2015 = ee.Image("users/hinojohinojo/human_modification_index/gHMv1_4_300m_2015_cland")
  .divide(65536);
var humanMask = hmi2015.subtract(hmi1990).lt(0.1);

// 9 is the 75 percentil of human footprint index
// 0.4 is considered high impact in human modification index
// 0.1 is a good value for the difference

// Get an average global LMAslope
var LMAslopeWithHumans = LMAslope.updateMask(humanMask);
var LMAslopeMeanHighHuman = LMAslopeWithHumans.reduceRegion({
  reducer: ee.Reducer.mean(),
  scale: 1000,
  geometry: region,
  crs: proj,
  maxPixels: 500000000
});
print("Slope Humans", LMAslopeMeanHighHuman);

Map.addLayer({
  eeObject: humanMask,
  visParams: {min: 0, max: 1},
  name: 'humanMask'
});

// print(ui.Chart.image.histogram({
//   image: human,
//   region: region,
//   scale: 1000,
//   maxPixels: 250000000
// }));



