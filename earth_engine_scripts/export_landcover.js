var region = /* color: #d63000 */ee.Geometry.Polygon(
        [[[-170.8054441654845, 59.67256615354733],
          [-167.75699343403835, 63.12444699330752],
          [-169.3991941654845, 68.75577777945934],
          [-151.8210691654845, 72.26522112584334],
          [-133.5398191654845, 70.17344209236731],
          [-128.3668695915073, 70.74249239792974],
          [-119.1257566654845, 69.99380453699892],
          [-115.17387345370504, 68.9445740453455],
          [-111.61985141573605, 68.07526879880018],
          [-106.7301324878077, 69.01149493731087],
          [-101.02028791548435, 68.14240963848385],
          [-98.02887577268109, 69.97867262944125],
          [-82.78298322798435, 69.82776636087881],
          [-79.66972905995323, 68.78343640674383],
          [-78.5420580506215, 64.14592998301984],
          [-61.1179441654845, 59.84961491939881],
          [-44.2429441654845, 55.92993951489615],
          [-5.922631665484506, 60.02572679078852],
          [23.257055834515494, 71.38819217222006],
          [32.04611833451549, 71.38819217222006],
          [41.53830583451549, 69.2595336197146],
          [53.796226428830316, 70.02838048268589],
          [58.7391188503717, 70.59129446203625],
          [63.68674333451549, 70.58659401933943],
          [68.9601808345155, 73.80298394344086],
          [104.90744645951565, 77.78821201084762],
          [119.58518083451548, 76.41621759005494],
          [118.17893083451548, 74.66254417256272],
          [132.24143083451548, 73.9007607722116],
          [135.75705583451548, 77.06125573671632],
          [160.28330412820782, 77.10053583268643],
          [156.14768083451548, 74.84742672815219],
          [159.15095054498394, 71.85819565924614],
          [-176.5969966319951, 71.71455382879637],
          [-172.91481916548452, 67.9782173542937],
          [-169.75075666548452, 66.55206624089212],
          [-168.20085755236718, 62.81452364393511],
          [-173.44216291548452, 62.47614004846054],
          [174.95627458451548, 60.461929272914006],
          [162.82736833451548, 51.35030732838795],
          [135.40549333451548, 27.302181024312347],
          [128.02268083451548, 17.233885415705853],
          [131.88986833451548, 7.2847713192719565],
          [156.49924333451565, -3.240667870624536],
          [-174.83119822525384, -14.009993893607566],
          [178.29611833451565, -48.69096039092549],
          [148.76486833451565, -47.27922900257082],
          [126.96799333451548, -37.71859032558814],
          [108.68674333451548, -36.597889133070204],
          [104.46799333451548, -11.178401873711772],
          [87.5929933345155, 1.7575368113083254],
          [74.5851808345155, 2.4601811810210052],
          [64.0383058345155, 13.923403897723347],
          [53.13986833451549, 3.8642546157214084],
          [55.24924333451549, -15.961329081596647],
          [47.86643083451549, -30.751277776257812],
          [23.608618334515494, -37.439974052270564],
          [10.249243334515494, -34.016241889667015],
          [2.5148683345154943, -0.7031073524364783],
          [-16.821069165484506, 3.162455530237848],
          [-32.9929441654845, -1.7575368113083125],
          [-34.2234129154845, -12.897489183755892],
          [-39.1452879154845, -23.563987128451217],
          [-55.8445066654845, -37.996162679728116],
          [-64.2820066654845, -49.03786794532642],
          [-58.3054441654845, -50.17689812200105],
          [-53.7351316654845, -52.58970076871779],
          [-65.3366941654845, -56.6562264935022],
          [-74.4773191654845, -56.072035471800866],
          [-80.4538816654845, -49.95121990866204],
          [-75.8835691654845, -18.47960905583197],
          [-85.3757566654845, -5.441022303717961],
          [-93.4616941654845, 0.17578097424708533],
          [-91.7038816654845, 11.350796722383672],
          [-109.6335691654845, 17.14079039331665],
          [-130.82089184302578, 35.71015549422054],
          [-145.4395328689418, 21.251724988635942],
          [-151.15737362389638, 15.278003453521686],
          [-162.7195066654845, 17.811456088564523],
          [-162.19429851063066, 23.83737996684216],
          [-149.4573153477156, 23.380169490440554],
          [-130.46774343603332, 44.27960394676777],
          [-138.90524343603332, 57.09074087203263],
          [-165.62399343603332, 50.46011176105434],
          [179.32677034388706, 50.54220125865511],
          [169.24831356296252, 52.784613034720735],
          [-174.06149343603332, 54.31891248123546]]]);


var vegClasses = ee.ImageCollection('COPERNICUS/Landcover/100m/Proba-V-C3/Global')
                .filterDate('2019-01-01', '2019-12-31')
                .select('discrete_classification')
                .first();

var vegQaMask = ee.ImageCollection('COPERNICUS/Landcover/100m/Proba-V-C3/Global')
                .filterDate('2019-01-01', '2019-12-31')
                .select('discrete_classification-proba')
                .first()
                .gte(50);

var coords = ee.Image.pixelLonLat();
print(coords);

// var equatorial = coords.select('latitude').gt(-5)
//   .and(coords.select('latitude').lt(5));
  
// var tropical = coords.select('latitude').gt(-23.43642)
//   .and(coords.select('latitude').lt(-5))
//   .or(coords.select('latitude').gt(5)
//     .and(coords.select('latitude').lt(23.43642))
//   );

var tropical = coords.select('latitude').gt(-23.43642)
  .and(coords.select('latitude').lt(23.43642));
  
var midLat = coords.select('latitude').gt(-66.56083)
  .and(coords.select('latitude').lt(-23.43642))
  .or(coords.select('latitude').gt(23.43642)
    .and(coords.select('latitude').lt(66.56083))
  );
  
var highLat = coords.select('latitude').gt(-85)
  .and(coords.select('latitude').lt(-66.56083))
  .or(coords.select('latitude').gt(66.56083)
    .and(coords.select('latitude').lt(85))
  );

Map.addLayer(highLat)

var vegClassesHighQA = vegClasses.updateMask(vegQaMask);

//var equatorialClasses = vegClassesHighQA.updateMask(equatorial);
var tropicalClasses = vegClassesHighQA.updateMask(tropical);
var midClasses = vegClassesHighQA.updateMask(midLat);
var highClasses = vegClassesHighQA.updateMask(highLat);

//Map.addLayer(vegClasses);
//Map.addLayer(vegClassesHighQA);
// Map.addLayer(vegQaMask);
//Map.addLayer({eeObject: equatorialClasses, name: 'ecuatorial'});
Map.addLayer({eeObject: tropicalClasses, name: 'tropical'});
Map.addLayer({eeObject: midClasses, name: 'mid'});
Map.addLayer({eeObject: highClasses, name: 'high'});
Map.addLayer({eeObject: vegClassesHighQA.eq(111), name: 'LCOI'})  // Map a single veg type of interest
//Map.addLayer(region);

//print(vegClasses)

// Define the Mollweide projection
// var proj = ' \
//   PROJCS["World_Mollweide", \
//     GEOGCS["GCS_WGS_1984", \
//       DATUM["WGS_1984", \
//         SPHEROID["WGS_1984",6378137,298.257223563]], \
//       PRIMEM["Greenwich",0], \
//       UNIT["Degree",0.017453292519943295]], \
//     PROJECTION["Mollweide"], \
//     PARAMETER["False_Easting",0], \
//     PARAMETER["False_Northing",0], \
//     PARAMETER["Central_Meridian",0], \
//     UNIT["Meter",1], \
//     AUTHORITY["EPSG","54009"]]';
    
var proj = 'EPSG:4326';
    
Export.image.toDrive({
  image: vegClassesHighQA,
  description: 'land_classes_1000m_WSGS84',
  folder: 'world_land_cover',
  fileNamePrefix: 'land_classes_1000m_WSGS84',
  region: region,
  maxPixels: 100000000000,
  scale: 1000,
  crs: proj,
  skipEmptyTiles: true,
  fileFormat: 'GeoTIFF'
});

// Export.image.toDrive({
//   image: equatorialClasses,
//   description: 'land_classes_1000m_WSGS84_equatorial',
//   folder: 'world_land_cover',
//   fileNamePrefix: 'land_classes_1000m_WSGS84_equatorial',
//   region: region,
//   maxPixels: 100000000000,
//   scale: 1000,
//   crs: proj,
//   skipEmptyTiles: true,
//   fileFormat: 'GeoTIFF'
// });

Export.image.toDrive({
  image: tropicalClasses,
  description: 'land_classes_1000m_WSGS84_tropical',
  folder: 'world_land_cover',
  fileNamePrefix: 'land_classes_1000m_WSGS84_tropical',
  region: region,
  maxPixels: 100000000000,
  scale: 1000,
  crs: proj,
  skipEmptyTiles: true,
  fileFormat: 'GeoTIFF'
});

Export.image.toDrive({
  image: midClasses,
  description: 'land_classes_1000m_WSGS84_mid',
  folder: 'world_land_cover',
  fileNamePrefix: 'land_classes_1000m_WSGS84_mid',
  region: region,
  maxPixels: 100000000000,
  scale: 1000,
  crs: proj,
  skipEmptyTiles: true,
  fileFormat: 'GeoTIFF'
});

Export.image.toDrive({
  image: highClasses,
  description: 'land_classes_1000m_WSGS84_high',
  folder: 'world_land_cover',
  fileNamePrefix: 'land_classes_1000m_WSGS84_high',
  region: region,
  maxPixels: 100000000000,
  scale: 1000,
  crs: proj,
  skipEmptyTiles: true,
  fileFormat: 'GeoTIFF'
});

// var vegClasses = ee.Image(0)
//   // conifers will be value 1
//   .where(vegClasses.eq(111)  // Closed evergreen conifer forest
//     .or(vegClasses.eq(113))  // Closed deciduous conifer forest
//     .or(vegClasses.eq(121))  // Open evergreen conifer forest
//     .or(vegClasses.eq(123)),  // Open deciduous conifer forest
//     1)
  
//   // broadleaf will be value 2
//   .where(vegClasses.eq(112)  // Closed evergreen broadleaf forest                    
//     .or(vegClasses.eq(114))  // Closed deciduous broadleaf forest                    
//     .or(vegClasses.eq(122))  // Open evergreen broadleaf forest                    
//     .or(vegClasses.eq(124)),  // Open deciduous broadleaf forest 
//     2)
  
//   // mixed will be value 3
//   .where(vegClasses.eq(115)  // Closed mixed forest                    
//     .or(vegClasses.eq(125)),  // Open mixed forest  )
//     3)
    
//   // herbaceous will be value 4
//   .where(vegClasses.eq(30),  // Herbaceous                    
//     //.or(vegClasses.eq(100)),  // Moss and Lichen  
//     4)
//   .rename('vegClasses')
// ;






